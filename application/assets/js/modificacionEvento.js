function seleccionarTipoParticipante(id){
	$('#tipoParticipanteCosto'+id).attr('style', 'display:block');
	$('#tipoParticipante'+id + '> div > input[type="checkbox"]').attr('onclick','quitarTipoParticipante('+id+')');
	$('#tipoParticipanteValor'+id+ '>input').blur(function() {
		  if($('#tipoParticipanteValor'+id+ '>input').val()==''){
			  $('#tipoParticipanteValor'+id+ '>input').val('0.00');
		  }
		});
}

/**
 * Valida extensión de imagen
 * @param  string value
 * @return boolean
 */
$.fn.form.settings.rules.imagen=function(value) {
	if (value=='' || value==null)
		return true;
	var tiposPermitidos=['jpg', 'png', 'gif', 'jpeg', 'bmp'];
	var res = value.split(".");
	var tipoDocumento=res[res.length-1];
	if (jQuery.inArray(tipoDocumento, tiposPermitidos)>=0)
		return true;
	return false;
}

function quitarTipoParticipante(id){
	$('#tipoParticipanteCosto'+id).attr('style', 'display:none');
	$('#tipoParticipante'+id + '> div > input[type="checkbox"]').attr('onclick','seleccionarTipoParticipante('+id+')');
}

function requiereCertificado(){
	$('#documentoCertificado').attr('style', 'display:block');
	$('#certificadoCheckbox').attr('onclick','noRequiereCertificado()');
}

function noRequiereCertificado(){
	$('#documentoCertificado').attr('style', 'display:none');
	$('#certificadoCheckbox').attr('onclick','requiereCertificado()');
}
function requiereCertificadoMod(obj){
	if($(obj).is(':checked')) {
           $('#documentoCertificado').attr('style', 'display:block');
		   $('#documentoCertificado').attr('required', true);
        } else {
           $('#documentoCertificado').attr('style', 'display:none');
		   $('#documentoCertificado').attr('required', false);
        }
}

$('body').on('click','img',function(){
	var id=($(this).attr("id"));
	if (id=='imagenCertificado'){
		 $('#modaldiv').modal('show');
	}

  });

$.fn.form.settings.rules.countTipoParticipante= function(value) {
	if($('#idTipoParticipante:checked').length > 0) return true;
	else return false;
}


$.fn.form.settings.rules.requiereCertficado= function(value) {
	if($('#certificadoCheckbox:checked').length > 0 && $('#documento').val()==''){
		return false;
	}
	return true;
}

$.fn.form.settings.rules.fechaInicio=function(value) {
	var fechaInicio=moment($('#fechaInicioEvento').val());
	if (fechaInicio.diff(moment.now())<0){
		return false
	}
	return true
}

$.fn.form.settings.rules.docx=function(value) {
	var documento=$('#documento').val();
	var tipoDocumento=documento.substr(documento.length - 4);
	if ($('#certificadoCheckbox:checked').length > 0 && tipoDocumento!="docx")
		return false
	return true;
}

$.fn.form.settings.rules.fechaFin=function(value) {
	var fechaInicio=moment($('#fechaInicioEvento').val());
	var fechaFin=moment($('#fechaFinEvento').val());
	if (fechaFin.diff(fechaInicio)<0){
		return false
	}
	return true
}


var formulario
$( document ).ready(function() {
 formulario=$('.ui.form')
	.form({
	  fields: {
		nombreEvento: {
	      identifier: 'nombreEvento',
	      rules: [
	        {
	          type   : 'empty',
	          prompt : 'Por favor ingresar detalle de evento'
	        }
	      ]
	    },
	    documentoCertificado: {
	      identifier: 'plantillaEvento',
	      rules: [
	        {
	          type   : 'requiereCertficado',
	          prompt : 'Por favor ingresar documento'
	        }
	      ]
	    },
			imagenEvento: {
		      identifier: 'imagenEvento',
		      rules: [
						{
		          type   : 'imagen',
		          prompt : 'La imagen debe ser png, jpg o gif'
		        }
		      ]
				},
	    documentoCertificado: {
		      identifier: 'plantillaEvento',
		      rules: [
		        {
		          type   : 'docx',
		          prompt : 'El ddocumento debe ser un documento de word con estensión .docx'
		        }
		      ]
		    },
	    tipoParticipante: {
	        identifier: 'idTipoParticipante',
	        rules: [
	          {
	        	type : 'countTipoParticipante',
	            prompt   : 'Debe ingresar por lo menos un tipo de participante'
	          }
	        ]
	    },
	    fechaInicio: {
	        identifier: 'fechaInicioEvento',
	        rules: [
	          {
		        	type : 'empty',
		            prompt   : 'Debe ingresar sus Fecha de inicio'
		      }
	        ]
	    },
	    fechaFin: {
	        identifier: 'fechaFinEvento',
	        rules: [
	          {
		        	type : 'empty',
		            prompt   : 'Debe ingresar sus Fecha de fin'
		      },
	          {
	        	type : 'fechaFin',
	            prompt   : 'La fecha de finalización del evento debe ser posterior a la fehca de inicio'
	          }
	        ]
	    },
	    observacionesModificacion:{
	    	identifier: 'observacionesModificacion',
	        rules: [
	          {
	        	type : 'empty',
	            prompt   : 'Debe ingresar sus observaciones'
	          }
	        ]
	    }

	  },
	  inline: true
	});

		/**
		 * Selecciona parámetros de date picker de fecha de evento
		 */
		$('#fechaInicioEvento').datetimepicker({
			value: '' + moment().format('YYYY/MM/DD HH:00'),
			allowTimes:[
				  '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
				  '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30',
				  '20:00', '20:30'
				 ]
		});

		/**
		 * Selecciona parámetros de date picker de fin de evento
		 */
		$('#fechaFinEvento').datetimepicker({
			value: '' + moment().format('YYYY/MM/DD HH:00'),
			allowTimes:[
				  '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
				  '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30',
				  '20:00', '20:30', '21:00', '21:30'
				 ]
		});
});
