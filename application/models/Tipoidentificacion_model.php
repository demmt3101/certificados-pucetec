<?php
/**
 * Clase para tipos de identificadores
 * @author TICs
 *
 */

class Tipoidentificacion_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}
	/**
	 * Obtiene todos los tipos de participante
	 */
	public function obtenerTiposIdentificacion(){
		$this->db->select('*');
		$this->db->from('tipoidentificacion');
		$this->db->where('estadoTipoIdentificacion', 1);
		$this->db->order_by('descripcionTipoIdentificacion', 'ASC');
		$query=$this->db->get();
		return $query->result_array();
	}
	
	/**
	 * Obtiene el nombre de tipo de identificación
	 * @param int $idTipoIdentificacion
	 */
	public function obtenerDescripcionTipoIdentificacion($idTipoIdentificacion){
		$this->db->select('descripcionTipoIdentificacion');
		$this->db->from('tipoidentificacion');
		$this->db->where('idTipoIdentificacion', $idTipoIdentificacion);
		$query=$this->db->get();
		return $query->row_array();
	}
}
?>