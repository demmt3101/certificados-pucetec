<?php

/**
 * Clase para administrar la relación entre participante y evento
 * @author TICs
 *
 */
class Participanteevento_model extends CI_Model {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}


	/**
	 * Obtiene el registro de inscripción de un participante a un evento
	 * @param int $idParticipanteEvento
	 */
	public function obtenerParticipanteEvento($idParticipanteEvento){
		$this->db->select('*');
		$this->db->from('participanteevento');
		$this->db->where('idParticipanteEvento', $idParticipanteEvento);

		$query=$this->db->get();
		return $query->row_array();
	}

	/**
	 * Obtiene el registro de inscripción de un participante a un evento
	 * @param int $idParticipanteEvento
	 */
	public function obtenerParticipanteEventoConfirmado($idParticipanteEvento){
		$this->db->select('*');
		$this->db->from('participanteevento');
		$this->db->where('idParticipanteEvento', $idParticipanteEvento);
		$this->db->where('estadoParticipanteEvento', 1);

		$query=$this->db->get();
		return $query->row_array();
	}

	/**
	 * Ingresa a la base de datos el documento cargado para ingreso de comprobante de pago
	 * @param int $idParticpanteEvento
	 * @param int $archivoComprobantePago
	 */
	public function ingresarComprobantePago($idParticpanteEvento, $archivoComprobantePago){
		$data=array(
				'rutaEvidenciaPago' => $archivoComprobantePago
		);
		$this->db->where('idParticipanteEvento', $idParticpanteEvento);
		$this->db->update('participanteevento', $data);
	}

	/**
	 * Actualiza la información acerca de los pagos de un participante inscrito a un evento específico
	 * @param int $idParticipanteEvento
	 */
	public function actualizarDatosFinancieros($idParticipanteEvento){
		$data = array(
				'facturaCiespalParticipanteEvento' => $this->input->post('facturaCiespalParticipanteEvento'),
				'valorPagadoParticipanteEvento' => $this->input->post('valorPagadoParticipanteEvento'),
				'confirmadoPorParticipanteEvento'=> $this->input->post('confirmadoPorParticipanteEvento'),
				'fechaConfirmadoPorParticipanteEvento' => date('Y-m-d H:i:s',now()),
				'observacionesParticipanteEvento' => $this->input->post('observacionesParticipanteEvento'),
				'estadoParticipanteEvento' => $this->input->post('estadoParticipanteEvento')
		);

		$this->db->where('idParticipanteEvento', $idParticipanteEvento);

		return $this->db->update('participanteevento', $data);
	}

	/**
	 * Cambia el estado de la relación participante-evento
	 * @param int $idParticipanteEvento
	 * @param string $nombreArchivo
	 */
	public function cambiarEstado($idParticipanteEvento, $estado){
			$data = array(
					'estadoParticipanteEvento' => $estado,
					'observacionesParticipanteEvento' => $this->input->post('observacionesParticipanteEvento')
			);
			$this->db->where('idParticipanteEvento', $idParticipanteEvento);
			return $this->db->update('participanteevento', $data);
	}


	/**
	 * Cambia el valor de entrega de certificado a 1
	 * @param int $idParticipanteEvento
	 */
	public function guardarEntregaCertificado($idParticipanteEvento){
		$data = array('entregaCertificadoParticipanteEvento' => 1);
		$this->db->where('idParticipanteEvento', $idParticipanteEvento);
		return $this->db->update('participanteevento', $data);
	}

	/**
	 * Revisa en la base de datos si se ha entregado anteriormente un certificado
	 * @param int $idParticipanteEvento
	 * @return boolean
	 */
	public function revisarEntregaCerficado($idParticipanteEvento){
		$this->db->select('entregaCertificadoParticipanteEvento');
		$this->db->from('participanteevento');
		$this->db->where('idParticipanteEvento', $idParticipanteEvento);

		$query=$this->db->get();
		if ($query->row_array()['entregaCertificadoParticipanteEvento']==1)
			return true;
		else
			return false;
	}

	/**
	 * Cambia el valor de entrega de solapín de 0 a 1
	 * @param int $idParticipanteEvento
	 */
	public function guardarEntregaSolapin($idParticipanteEvento){
		$data = array('solapinParticipanteEvento' => 1);
		$this->db->where('idParticipanteEvento', $idParticipanteEvento);
		return $this->db->update('participanteevento', $data);
	}

	/**
	 * Revisa en la base de datos si se ha entregado anteriormente un solapín
	 * @param int $idParticipanteEvento
	 * @return boolean
	 */
	public function revisarEntregaSolapin($idParticipanteEvento){
		$this->db->select('solapinParticipanteEvento');
		$this->db->from('participanteevento');
		$this->db->where('idParticipanteEvento', $idParticipanteEvento);

		$query=$this->db->get();
		if ($query->row_array()['solapinParticipanteEvento']==1)
			return true;
			else
				return false;
	}

	/**
	 * Almacena en la base de datos el documento PDF guardado correspondiente al certificado entregado al participante que asiste a un evento específico
	 * @param int $idParticipanteEvento
	 * @param string $nombreArchivo
	 */
	public function almacenarCertificado($idParticipanteEvento, $nombreArchivo){
		$data = array(
				'rutaCertificadoParticipanteEvento' => $nombreArchivo
		);
		$this->db->where('idParticipanteEvento', $idParticipanteEvento);

		$this->db->update('participanteevento', $data);

		return $nombreArchivo;
	}

	/**
	 * Actualiza el valor a pagar de un participante inscrito en un evento
	 * @param int $idParticipanteevento
	 * @param float $valorAPagar
	 * @return boolean
	 */
	public function actualizarValorAPagar($idParticipanteevento, $valorAPagar){
		$data = array(
				'valorAPagar' => $valorAPagar,
		);
		$this->db->where('idParticipanteEvento', $idParticipanteevento);
		return $this->db->update('participanteevento', $data);
	}

	/**
	 * Actualiza número de factura de un registro de factura de un evento
	 * @param int $idParticipanteevento
	 * @return boolean
	 */
	public function actualizarFactura($idParticipanteevento){
		$data = array(
				'facturaCiespalParticipanteEvento' => $this->input->post('numeroFactura'),
		);
		$this->db->where('idParticipanteEvento', $idParticipanteevento);
		return $this->db->update('participanteevento', $data);
	}

	/**
	 * Comprueba si un participante está inscrito en un evento o no
	 * @param  int $idParticipante ID del participante
	 * @param  int $idEvento       ID del evento
	 * @return boolean
	 */
	public function comprobarParticipanteEvento($idParticipante, $idEvento){
		$condiciones = array('idParticipante' => $idParticipante,
												 'idEvento'=>$idEvento,
											 	 'estadoParticipanteEvento' => 3);
		$participanteevento = $this->db->get_where('participanteevento', $condiciones);
		if ($participanteevento->num_rows() > 0)
			return true;
		return false;
	}
}
