<?php
/**
 * Modelo para CRUD y otras operaciones de participantes
 * @author TICs
 *
 */

class Participante_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}

	/**
	 * Obtiene listado de participantes o un participante específico
	 * @param integer $participanteId
	 */
	public function obtenerParticipante($idParticipante = FALSE){
		if ($idParticipante === FALSE){
			$this->db->select('*');
			$this->db->from('participante');
			$this->db->where('estadoParticipante', 1);
			$this->db->order_by('apellidosParticipante', 'ASC');
			$this->db->order_by('nombresParticipante', 'ASC');
			$query=$this->db->get()->result_array();;
			return $query->result_array();
		}
		return $this->db->get_where('participante', array('idParticipante' => $idParticipante, 'estadoParticipante' => 1))->row_array();
//		return $query->row_array();
	}

	/**
	 * Obtiene listado de todas los números de identifiaciones e emails de todos los participantes
	 * @param integer $participanteId
	 */
	public function obtenerIdentificaciones(){
		$this->db->select('identificacionParticipante, correoElectronicoParticipante');
		$this->db->from('participante');
		$this->db->where('estadoParticipante', 1);
		$this->db->order_by('apellidosParticipante', 'ASC');
		$this->db->order_by('nombresParticipante', 'ASC');
		$query=$this->db->get();
		return $query->result_array();
	}

	/**
	 * Obtiene listado de participantes inscritos en un evento específico
	 * @param integer $eventoId
	 */
	public function obtenerParticipanteEvento($eventoId){

		$this->db->distinct('*');
		$this->db->from('participante');
		$this->db->join('participanteevento', 'participante.idParticipante = participanteevento.idParticipante');
		$this->db->where('participanteevento.idEvento', $eventoId);
		$this->db->where('participanteevento.estadoParticipanteEvento!=', 0);
		$this->db->order_by('apellidosParticipante', 'ASC');
		$this->db->order_by('nombresParticipante', 'ASC');
		$query=$this->db->get();
		return $query->result_array();

	}
	/**
	 * Login Participante
	 * @param string $email
	 * @param string $contrasena
	 */
	public function loginParticipante($email, $contrasena){
		$this->db->select('*');
		$this->db->from('participante');
		$this->db->where('correoElectronicoParticipante', $email);
		$this->db->where('contrasenaParticipante', $contrasena);
		$this->db->where('estadoParticipante', 1);

		$query=$this->db->get();

		if ( $query->num_rows() == 0)
			return null;

			return $query->row_array();
	}

	/**
	 * Devuelve un arreglo con todos los datos de facturación que ha ingresado el participante. En el caso que ingrese id de evento obtendrá los datos de facturación para un evento específico
	 * @param int $idParticipante
	 * @param int $idEvento
	 */
	public function obtenerDatosFacturacion($idParticipante, $idEvento=FALSE) {
		if ($idEvento!=FALSE){
			$this->db->select('*');
			$this->db->from('datosfacturacion');
			$this->db->join('participanteevento','participanteevento.idDatosFacturacion=datosfacturacion.idDatosFacturacion');
			$this->db->where('participanteevento.idParticipante', $idParticipante);
			$this->db->where('participanteevento.estadoParticipanteEvento!=', 0);
			$this->db->where('idEvento', $idEvento);
			$query=$this->db->get();
			return $query->row_array();
		}

		$this->db->select('*');
		$this->db->from('datosfacturacion');
		$this->db->where('idParticipante', $idParticipante);
		$query=$this->db->get();
		return $query->result_array();
	}


	/**
	 * Crea un nuevo participante
	 */
	public function crearParticipante($documentoDescuento=NULL){
		if ($this->input->post('descuento')!=null && $this->input->post('descuento')!=0)
			$idDescuento=$this->input->post('descuento');
		else
			$idDescuento=null;
		$apellidosParticipante=$this->input->post('apellidosParticipante');
		$nombresParticipante=$this->input->post('nombresParticipante');
		$correoElectronicoParticipante=$this->input->post('correoElectronicoParticipante');
		$contrasenaParticipante=$this->input->post('contrasenaParticipante');
		$identificacionParticipante=$this->input->post('identificacionParticipante');
		$institucionParticipante=$this->input->post('institucionParticipante');
		$ciudadParticipante=$this->input->post('ciudadParticipante');
		$telefonoParticipante=$this->input->post('telefonoParticipante');
		if($this->input->post('extensionTelefonoParticipante')!=null || $this->input->post('extensionTelefonoParticipante')!=''){
			$telefonoParticipante=$telefonoParticipante.' ext. '.$this->input->post('extensionTelefonoParticipante');
		}
		$telefono2Participante=$this->input->post('telefono2Participante');
		if($this->input->post('extensionTelefono2Participante')!=null || $this->input->post('extensionTelefono2Participante')!=''){
			$telefono2Participante=$telefono2Participante.' ext. '.$this->input->post('extensionTelefono2Participante');
		}
		$idPais=$this->input->post('idPais');
		$idTipoIdentificacion=$this->input->post('idTipoIdentificacion');

		$data = array (
				'apellidosParticipante' => $apellidosParticipante,
				'nombresParticipante' => $nombresParticipante,
				'correoElectronicoParticipante' => $correoElectronicoParticipante,
				'contrasenaParticipante' => md5($contrasenaParticipante),
				'identificacionParticipante' => $identificacionParticipante,
				'telefonoParticipante' => $telefonoParticipante,
				'telefono2Participante' => $telefono2Participante,
				'institucionParticipante' => $institucionParticipante,
				'ciudadParticipante' => $ciudadParticipante,
				'fechaIngresoParticipante' => date('Y-m-d H:i:s',now()),
				'idPais' => $idPais,
				'idTipoIdentificacion' => $idTipoIdentificacion,
				'idDescuentoRegistrado' => $idDescuento,
				'documentoDescuento' => $documentoDescuento,
				'estadoParticipante' => 1
		);

		$this->db->insert('participante', $data);
		return $this->db->insert_id();
	}

	/**
	 * Actualiza el campo de contraseña en la base de datos
	 * @param int $idParticipante
	 * @param string $contrasena
	 */
	public function actualizarContrasena($idParticipante, $contrasena){
		$data = array(
				'contrasenaParticipante' => $contrasena,
		);
		$this->db->where('idParticipante', $idParticipante);
		$this->db->update('participante', $data);
	}

	/**
	 * obtiene un participante, buscando por su correo electrónico
	 * @param string $correoElectronicoParticipante
	 */
	public function obtenerParticipantePorCorreo($correoElectronicoParticipante){
		$query = $this->db->get_where('participante', array('correoElectronicoParticipante' => $correoElectronicoParticipante));
		return $query->row_array();
	}

	/**
	 * Verifica en la base de datos la existencia de un corre electrónico y devuelve un entero
	 * @param string $email
	 */
	public function verificarEmail($email){
		$this->db->where('correoElectronicoParticipante', $email);
		$this->db->from('participante');
		return $this->db->count_all_results();
	}

	/**
	 * Elimina el descuento del participante
	 * @param int $idParticipante
	 */

	public function quitarDescuento($idParticipante){
		$data = array(
				'idDescuentoRegistrado' => null,
				'documentoDescuento' => ''
		);
		$this->db->where('idParticipante', $idParticipante);
		$this->db->update('participante', $data);
	}
}
?>
