<?php
/**
 * Modelo para formas de pago
 * @author TICS
 *
 */
class Tipoformapago_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}
	
	/**
	 * Enlista los métdos de pago en orden alfabético
	 */
	public function obtenerFormaPago(){
		$this->db->select('*');
		$this->db->from('tipoformapago');
		$this->db->where('estadoTipoFormaPago', 1);
		$this->db->order_by('descripcionTipoFormaPago', 'ASC');
		$query=$this->db->get();
		return $query->result_array();
	}
	
	public function obtenerDescripcionFormaPago($idformaPago){
		$this->db->select('descripcionTipoFormaPago');
		$this->db->from('tipoformapago');
		$this->db->where('idTipoFormaPago', $idformaPago);
		$query=$this->db->get();
		return $query->row_array();
	}
}