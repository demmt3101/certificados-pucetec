<?php

/**
 * Modelo de Evento
 * @author tics
 *
 */

class Evento_model extends CI_Model {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}

	/**
	 * Obtiene listado de eventos o un evento específico
	 * @param integer $eventoId
	 */
	public function obtenerEventos($eventoId = FALSE){
		if ($eventoId === FALSE){
			$this->db->select('*');
			$this->db->from('evento');
			$this->db->where('estadoEvento', 1);
			$this->db->order_by('fechaInicioEvento', 'DESC');
			$query=$this->db->get();
			return $query->result_array();
		}
		$query = $this->db->get_where('evento', array(
					'idEvento' => $eventoId,
					'estadoEvento' => 1
		));
		return $query->row_array();
	}

	/**
	 * Ingreso en base de datos de Evento
	 */
	public function insertarEvento(){

		$idTipoEvento=$this->input->post('tipoEvento');
		$nombreEvento=$this->input->post('nombreEvento');
		$fechaInicioEvento=$this->input->post('fechaInicioEvento');
		$fechaFinEvento=$this->input->post('fechaFinEvento');
		$descripcionEvento=$this->input->post('descripcionEvento');
		$certificadoEvento=0;
		if($this->input->post('certificadoEvento')!=null)
			$certificadoEvento=1;
		$cupoEvento=$this->input->post('cupoEvento');

		$eventoData = array(
				'tituloEvento' => preg_replace('#<[^>]+>#', ' ',$nombreEvento),
				'idTipoEvento' => $idTipoEvento,
				'fechaInicioEvento' => $fechaInicioEvento,
				'fechaFinEvento' => $fechaFinEvento,
				'certificadoEvento' => $certificadoEvento,
				'cupoEvento' => $cupoEvento,
				'idEmpleadoCreacion' => $this->session->userdata('idEmpleado'),
				'descripcionEvento' => $descripcionEvento,
				'estadoEvento' => 1
		);
		$this->db->insert('evento', $eventoData);

		$idEvento=$this->db->insert_id();

		$tipoParticipantes=$this->input->post('idTipoParticipante');

		foreach ($tipoParticipantes as $idTipoParticipante){

			$valoresEvento = array (
					'idTipoParticipante' => $idTipoParticipante,
					'costoValorEvento' => $this->input->post('tipoParticipanteValor'.$idTipoParticipante),
					'idEvento' => $idEvento,
					'estadoValorEvento' => 1
				);
			$this->db->insert('valorevento', $valoresEvento);
		}
		return $idEvento;
	}

	/**
	 * añade la ruta de la plantilla a un evento
	 * @param int $idEvento
	 */
	public function ingresarPlantillaEvento($idEvento, $nombreArchivo){
		$data = array(
				'plantillaCertificadoEvento' => $nombreArchivo,
		);
		$this->db->where('idEvento', $idEvento);
		$this->db->update('evento', $data);
	}

	/**
	 * añade la ruta de la imagen a un evento
	 * @param int $idEvento
	 */
	public function ingresarImagenEvento($idEvento, $nombreArchivo){
		$data = array(
				'imagenEvento' => $nombreArchivo,
		);
		$this->db->where('idEvento', $idEvento);
		$this->db->update('evento', $data);
	}

	/**
	 * Obtiene los costos de un evento específico, dependiendo del tipo de participante
	 * @param id $idEvento
	 */
	public function obtenerCostos($idEvento){
		$this->db->select('descripcionTipoParticipante, costoValorEvento, tipoparticipante.idTipoParticipante');
		$this->db->from('tipoparticipante');
		$this->db->join('valorevento', 'valorevento.idTipoParticipante=tipoparticipante.idTipoParticipante');
		$this->db->where('valorevento.estadoValorEvento', 1);
		$this->db->where('valorevento.idEvento', $idEvento);
		$this->db->order_by('descripcionTipoParticipante', 'ASC');
		$query=$this->db->get();
		return $query->result_array();
	}

	/**
	 * Inscribe a un nuevo participante en un evento específico
	 * @param int $idEvento
	 */
	public function agregarParticipante($idEvento, $idParticipante){
		if ($this->input->post('idDatosFacturacion')=='Nueva'){
			$formaPago = array (
					'rucDatosFacturacion' => $this->input->post('rucDatosFacturacion'),
					'razonSocialDatosFacturacion' => $this->input->post('razonSocialDatosFacturacion'),
					'direccionDatosFacturacion' => $this->input->post('direccionDatosFacturacion'),
					'telefonoDatosFacturacion' => $this->input->post('telefonoDatosFacturacion'),
					'estadoDatosFacturacion' => 1,
					'idParticipante' => $idParticipante
			);

			$this->db->insert('datosfacturacion', $formaPago);
			$idDatosFacturacion=$this->db->insert_id();

		}else{
			$idDatosFacturacion=$this->input->post('idDatosFacturacion');
		}
		$participanteEvento= array(
			'fechaMatriculaParticipanteEvento' => date('Y-m-d H:i:s',now()),
			'certificadoParticipanteEvento' => $this->input->post('nombreCertificado'),
			'facturaCiespalParticipanteEvento' => '',
			'entregaCertificadoParticipanteEvento' => 0,
			'valorPagadoParticipanteEvento' => 0,
			'confirmadoPorParticipanteEvento' => 0,
			'observacionesParticipanteEvento'=>'',
			'valorAPagar' => $this->input->post('valorAPagar'),
			'solapinParticipanteEvento' => 0,
			'rutaEvidenciaPago' => '',
			'descripcionEvidenciaPago' => $this->input->post('descripcionEvidenciaPago'),
			'estadoParticipanteEvento' => 2,
			'idTipoParticipante' => $this->input->post('idTipoParticipante'),
			'idTipoFormaPago' => $this->input->post('idTipoFormaPago'),
			'idParticipante'=> $idParticipante,
			'idEvento'=> $idEvento,
			'idDatosFacturacion' => $idDatosFacturacion
		);
		echo "<pre>";
		print_r($participanteEvento);
		echo "</pre>";
		$this->db->insert('participanteevento', $participanteEvento);
		echo $this->db->last_query();
		return $this->db->insert_id();
	}

	/**
	 * Obtiene el número de participantes de evento
	 * @param int $idEvento
	 */

	public function obtenerNumeroParticipantesEvento($idEvento){
		$this->db->where('idEvento', $idEvento);
		$this->db->where('estadoParticipanteEvento!=', 0);
		$this->db->from('participanteevento');
		return  $this->db->count_all_results();
	}

	/**
	 * Obtiene el número de participantes confirmados de evento
	 * @param int $idEvento
	 */

	public function obtenerNumeroParticipantesConfirmados($idEvento){
		$this->db->where('idEvento', $idEvento);
		$this->db->where('confirmadoPorParticipanteEvento!=', '0');
		$this->db->from('participanteevento');
		return  $this->db->count_all_results();
	}

	/**
	 * Obtiene la sumatoria de los valores a pagar de los inscritos para obtener un monto a cobrar total del evento
	 * @param unknown $idEvento
	 */
	public function obtenerValorPorCobrar($idEvento){
		$this->db->select_sum('valorAPagar');
		$this->db->from('participanteevento');
		$this->db->where('estadoParticipanteEvento!=', 0);
		$this->db->where('idEvento', $idEvento);
		$valorPorCobrar=$this->db->get();
		return $valorPorCobrar->row_array()['valorAPagar'];
	}

	/**
	 * Obtiene el monto corbado de todos los participantes que ya han pagado
	 * @param int $idEvento
	 */

	public function obtenerMontoCobrado($idEvento){
		$this->db->select_sum('valorPagadoParticipanteEvento');
		$this->db->from('participanteevento');
		$this->db->where('estadoParticipanteEvento!=', 0);
		$this->db->where('idEvento', $idEvento);
		$valorPorCobrar=$this->db->get();
		return $valorPorCobrar->row_array()['valorPagadoParticipanteEvento'];
	}

	/**
	 * Devuelve el nombre del archivo de plantilla de certificado de un evento
	 * @param int $idEvento
	 */
	public function obtenerPlantillaCertificado($idEvento){
		$this->db->select('plantillaCertificadoEvento');
		$this->db->from('evento');
		$this->db->where('idEvento', $idEvento);

		$query=$this->db->get();
		return $query->row_array()['plantillaCertificadoEvento'];
	}

	/**
	 * Cambia la variable de estados eventos (0 => Evento Inactivo/Eliminado, 1 => Evento Activo)
	 * @param intt $idEvento
	 * @param int $estadoEvento
	 */
	public function cambiarEstadoEvento($idEvento, $estadoEvento, $idEmpleadoEliminacion=FALSE){
		if ($idEmpleadoEliminacion===FALSE){
			$data = array(
					'estadoEvento' => $estadoEvento,
			);
		}else{
			$data = array(
					'idEmpleadoEliminacion' => $idEmpleadoEliminacion,
					'observacionesEliminacion' => $this->input->post('observacionesEliminacion'),
					'estadoEvento' => $estadoEvento,
			);
		}
		$this->db->where('idEvento', $idEvento);
		$this->db->update('evento', $data);
	}
	/**
	 * Actualiza la informacion del Evento
	 * @param int $idEvento
	 * @param array $datos
	 */
	public function actualizarEvento($idEvento,$datos){
		$this->db->where('idEvento', $idEvento);
		$this->db->update('evento', $datos);
	}
	/**
	 * Obtiene los participantes del evento
	 * @param int $idEvento
	 */

	public function obtieneParticipantesEvento($idEvento){
		$condicionesWhere = array(
            'idEvento' => $idEvento  );
        $this->db->select('*')->from('participanteevento')->where($condicionesWhere);
		$this->db->join('participante', 'participante.idParticipante = participanteevento.idParticipante');
        $registros = $this->db->get();
        return $registros->result_array();
	}

	/**
	 * Verifica si un curso es o no gratuito
	 * @param $idEvento
	 */
	public function esGratuito($idEvento){
		$this->db->select('costoValorEvento');
		$this->db->from('valorevento');
		$this->db->where('idEvento', $idEvento);
		$this->db->order_by('costoValorEvento', 'ASC');

		$query=$this->db->get();
		$valores= $query->result_array();
		if($valores[0]['costoValorEvento']==0)
			return true;
		return false;
	}

	/**
	 * Aprueba la inscripción de todos los participantes de un evento
	 * @param unknown $idEvento
	 */
	public function aprobarParticipantesEvento($idEvento){
		$data = array(
				'estadoParticipanteEvento' => 1,
				'confirmadoPorParticipanteEvento' => $this->session->userdata('idEmpleado'),
				'observacionesParticipanteEvento' => 'Aprobado por en masa, evento gratuito',
		);
		$this->db->where('idEvento', $idEvento);
		$this->db->where('valorAPagar=', 0);
		$this->db->update('participanteevento', $data);
	}

	/**
	 * Obtiene el costo original de un evento específicon con respecto a un tipo de participante específico
	 * @param int $idEvento
	 * @param int $idTipoParticipante
	 * @return float
	 */
	public function obtenerValorTipoParticipante($idEvento,$idTipoParticipante){
		$this->db->select('costoValorEvento');
		$this->db->from('valorevento');
		$this->db->where('idEvento', $idEvento);
		$this->db->where('idTipoParticipante', $idTipoParticipante);

		$query=$this->db->get();
		return $query->row_array()['costoValorEvento'];
	}

	/**
	 * Obtiene eventos posteriores a la fecha actual
	 * @return array
	 */
	public function obtenerProximosEventos(){
		$this->db->select('*');
		$this->db->from('evento');
		$this->db->where('estadoEvento', 1);
		$this->db->where('fechaInicioEvento>=', date('Y-m-d H:i:s',now()));
		$this->db->order_by('fechaInicioEvento', 'DESC');
		$query=$this->db->get();
		return $query->result_array();
	}

	/**
	 * Obtiene eventos que se encuentran en trascurso
	 * @return array
	 */
	public function obtenerEventosEnCurso(){
		$this->db->select('*');
		$this->db->from('evento');
		$this->db->where('estadoEvento', 1);
		$this->db->where('fechaInicioEvento>=', date('Y-m-d H:i:s',now()));
		$this->db->where('fechaFinEvento<=', date('Y-m-d H:i:s',now()));
		$this->db->order_by('fechaInicioEvento', 'DESC');
		$query=$this->db->get();
		return $query->result_array();
	}


	/**
	 * Obtiene CUATRO eventos posteriores MÁS CERCANOS a la fecha actual
	 * @return array
	 */
	public function obtenerCuatroProximosEventos(){
		$this->db->select('*');
		$this->db->from('evento');
		$this->db->where('estadoEvento', 1);
		$this->db->where('fechaInicioEvento>=', date('Y-m-d H:i:s',now()));
		$this->db->order_by('fechaInicioEvento', 'ASC');
		$this->db->limit(4);
		$query=$this->db->get();
		return $query->result_array();
	}
}
