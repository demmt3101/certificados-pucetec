<?php
/**
 * Modelo para gestión de asistencias de usuarios a eventos
 * @author TICs
 *
 */

class Asistencia_model extends CI_Model {
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('date');
	}
	
	/**
	 * Almacena en la base de datos la asistencia de un participante con su respectiva fecha
	 */
	
	public function registrarAsistencia($idParticipanteEvento, $idEmpleado){
		$data = array(
				'fechaAsistencia' => date('Y-m-d H:i:s',now()),
				'idParticipanteEvento' => $idParticipanteEvento,
				'idEmpleado' => $idEmpleado,
		);
		
		$this->db->insert('asistencia', $data);
		
		return $this->db->insert_id();
	}
	
	/**
	 * Devuelve un arreglo con el historial de asistencias
	 * @param inte $idParticianteEvento
	 */
	public function historial($idParticianteEvento){
		$this->db->distinct('*');
		$this->db->from('asistencia');
		$this->db->join('empleado', 'asistencia.idEmpleado = empleado.idEmpleado');
		$this->db->where('asistencia.idParticipanteEvento',$idParticianteEvento);
		$this->db->order_by('fechaAsistencia','ASC');
		
		$query=$this->db->get();
		return $query->result_array();
	}
}