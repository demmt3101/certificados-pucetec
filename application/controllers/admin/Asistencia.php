<?php

/**
 * Clase para administrar asistencias
 * @author TICs
 *
 */
class Asistencia extends CI_Controller{

	/**
	 * Constructor de clase Asistencia
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('asistencia_model');
		$this->load->model('participante_model');
		$this->load->model('participanteevento_model');
		$this->load->model('evento_model');
		$this->load->model('empleado_model');
	}

	/**
	 * Registra la asistencia de un participante a un evento
	 */
	public function registrarAsistencia($idEvento) {
		$asistencia=$this->input->post('asistencia');
		$data =  explode("<asistencia>",$this->input->post('datos'));
		$idParticipanteEvento=$data[0];
		$idEmpleado=$data[1];
		$participanteEvento=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		if($idEvento==$participanteEvento['idEvento'])
			$this->output->set_content_type('application/json')->set_output("".$this->asistencia_model->registrarAsistencia($idParticipanteEvento, $idEmpleado));
		else
			$this->output->set_content_type('application/json')->set_output("false");
	}

	/**
	 * Despliega El historial de asistenca de un participante a un evento específico
	 * @param int $idPaticipante
	 * @param int $idEvento
	 * @param int $idParticipanteEvento
	 */

	public function historial($idParticipante, $idEvento, $idParticipanteEvento){
		$data['evento']=$this->evento_model->obtenerEventos($idEvento);
		$data['participante']=$this->participante_model->obtenerParticipante($idParticipante);
		$data['historial']=$this->asistencia_model->historial($idParticipanteEvento);
		$data['titulo']="Historial";
		$this->load->view('/templates/admin/header', $data);
		$this->load->view('/admin/asistencia/historial', $data);
		$this->load->view('/templates/admin/footer');
	}

}
