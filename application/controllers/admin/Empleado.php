<?php

/**
 * Clase Empleado
 * @author tics
 */
class Empleado extends CI_Controller {

	/**
	 * Constructor de clase Empleado
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('empleado_model');
		$this->load->helper('text');
	}

	/**
	 * Despliegue de formulario de login de empleado
	 */
	public function index() {
		if ($this->session->userdata('idEmpleado')!=null)
				redirect('admin/evento');
		else {
			$email = $this->input->post("email");
			$contrasena = $this->input->post("contrasena");
			$empleado = $this->empleado_model->loginEmpleado($email, md5($contrasena));
			if ($empleado != null) {
				$this->session;
				$empleadoData = array(
						'idTipoEmpleado' => $empleado['idTipoEmpleado'],
						'email' => $empleado['correoElectronicoEmpleado'],
						'idEmpleado' => $empleado['idEmpleado'],
						'contrasena' => $empleado['contrasenaEmpleado'],
						'nombresEmpleado' => $empleado['nombresEmpleado'],
						'apellidosEmpleado' => $empleado['apellidosEmpleado']
				);

				$this->session->set_userdata($empleadoData);
				redirect('/admin/evento', 'refresh');
			} else {
						$data=null;
						if($email != null)
							$data['error'] = "Email y/o contraseñas incorrectos";
						$this->load->view('admin/login', $data);
			}
		}
	}

	/**
	 * Destuye sesión
	 */
	public function logout(){
		$this->session->sess_destroy();
		redirect('/admin', 'refresh');
	}
}
