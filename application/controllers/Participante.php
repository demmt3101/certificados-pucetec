<?php

/**
 * Clase Participante
 * @author tics
 */
class Participante extends CI_Controller {

	/**
	 * Constructor de clase Participante
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('participante_model');
		$this->load->model('participanteevento_model');
		$this->load->model('evento_model');
		$this->load->model('descuento_model');
		$this->load->model('pais_model');
		$this->load->model('tipoidentificacion_model');
		$this->load->model('tipoformapago_model');
		$this->load->helper('text');
	}

	/**
	 * Registrar loguear usuario para posterior inscripción en evento dentro de evento
	 * @param int $idEvento
	 */
	public function loginParticipante() {
		$email = $this->input->post("email");
		$contrasena = $this->input->post("contrasena");
		$participante = $this->participante_model->loginParticipante($email, md5($contrasena));
		$data['email'] = $email;
		if ($participante != null) {
			$ParticipanteData = array(
				'idTipoEmpleado' => null,
				'email' => $participante['correoElectronicoParticipante'],
				'idParticipante' => $participante['idParticipante'],
				'nombresParticipante' => $participante['nombresParticipante'],
				'apellidosParticipante' => $participante['apellidosParticipante'],
				'contrasena' => $participante['contrasenaParticipante'],
			);
			$this->session->set_userdata($ParticipanteData);
			redirect('/');
		} else {
				if ($email != null)
					$data['error'] = "Usuario y contraseña no válidos";
				$this->load->view('templates/header', $data);
				$this->load->view('participante/inicioSesion', $data);
				$this->load->view('templates/footer', $data);
		}
	}

	/**
	 * Registrar loguear usuario para posterior inscripción en evento dentro de evento
	 * @param int $idEvento
	 */
	public function login($idEvento) {
		$this->session->set_userdata('idEvento', $idEvento);
		$this->session->set_userdata('eventoSeleccionado', $idEvento);
		$data['evento']=$this->evento_model->obtenerEventos($idEvento);
		$data['numeroParticipantes'] = $this->evento_model->obtenerNumeroParticipantesEvento($idEvento);
		$data['titulo']=$data['evento']['tituloEvento'];
		$email = $this->input->post("email");
		$contrasena = $this->input->post("contrasena");
		$participante = $this->participante_model->loginParticipante($email, md5($contrasena));
		if ($participante != null) {
			$this->session;
			$ParticipanteData = array(
				'idTipoEmpleado' => null,
				'email' => $participante['correoElectronicoParticipante'],
				'idParticipante' => $participante['idParticipante'],
				'nombresParticipante' => $participante['nombresParticipante'],
				'apellidosParticipante' => $participante['apellidosParticipante'],
				'contrasena' => $participante['contrasenaParticipante'],
			);

			$this->session->set_userdata($ParticipanteData);
			redirect('/evento/inscribir');
		} else {
			if ($data['evento']!=null){
				if ($contrasena!=null){
					$data['error']="Usuario y contraseña no válidos";
					$this->load->view('templates/header', $data);
					$this->load->view('evento/despliegue', $data);
					$this->load->view('templates/footer', $data);
				}else{
					$this->load->view('templates/header', $data);
					$this->load->view('evento/despliegue', $data);
					$this->load->view('templates/footer', $data);
				}
			}else{
				$this->load->view('templates/header', $data);
				$this->load->view('templates/eventoInexistente', $data);
				$this->load->view('templates/footer', $data);
			}
		}
	}

	/**
	 * Registro de participante sin selección de evento
	 * @return [type] [description]
	 */
	public function registro () {
		$data['titulo']='Registro de usuario';
		$data['identificaciones']=$this->participante_model->obtenerIdentificaciones();
		$data['descuentos']=$this->descuento_model->obtenerNoGrupales();
		$data['paises']=$this->pais_model->obtenerPaises();
		$data['tiposIdentificacion']=$this->tipoidentificacion_model->obtenerTiposIdentificacion();
		$data['formasPago']=$this->tipoformapago_model->obtenerFormaPago();
		if($this->input->post('nombresParticipante') != null) {
				$idPartipante = $this->participante_model->crearParticipante();

				$this->notificarRegistro($idPartipante, $this->input->post('contrasenaParticipante'));
				$this->load->view('templates/header', $data);
				$this->load->view('/participante/registroExitoso', $data);
				$this->load->view('templates/footer');
			}else{
				$this->load->view('templates/header',$data);
				$this->load->view('participante/registro',$data);
				$this->load->view('templates/footer');
			}
	}

	/**
	 * Primer registro de participante cuando se ingresa desde un evento
	 * @param int $idEvento
	 */
	public function primerRegistro($idEvento){
		$data['identificaciones']=$this->participante_model->obtenerIdentificaciones();
		$data['evento']=$this->evento_model->obtenerEventos($idEvento);
		$data['titulo']='Registro de usuario para '. $data['evento']['tituloEvento'];
		$data['paises']=$this->pais_model->obtenerPaises();
		$data['tiposIdentificacion']=$this->tipoidentificacion_model->obtenerTiposIdentificacion();
			if($this->input->post('nombresParticipante')!=null){
				$idPartipante=$this->participante_model->crearParticipante();
				$idParticipanteEvento=$this->evento_model->agregarParticipante($idEvento, $idPartipante);					
				$this->load->view('templates/header', $data);
				echo '<div class="ui container">'.$this->upload->display_errors().'</div>';
				$this->load->view('templates/footer');
				$this->notificarInscripcionNuevoParticipante($idParticipanteEvento, $this->input->post('contrasenaParticipante'));
				$this->load->view('templates/header', $data);
				$this->load->view('/inscripcion/inscripcionExitosa', $data);
				$this->load->view('templates/footer');
			}else{
				$this->load->view('templates/header',$data);
				$this->load->view('inscripcion/usuarioNuevo',$data);
				$this->load->view('templates/footer');
			}
		}
	

	/**
	 * Notifica a un participante previamente inscrito cuando completa el formulario correctamente
	 * @param int $idParticipanteEvento
	 */
	protected function notificarInscripcionNuevoParticipante($idParticipanteEvento, $contrasena){
		$participanteEvento=$this->participanteevento_model->obtenerParticipanteEvento($idParticipanteEvento);
		$data['evento']=$this->evento_model->obtenerEventos($participanteEvento['idEvento']);
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($participanteEvento['idParticipante']);
		$data['contrasena']=$contrasena;
		$data['email']=$participante['correoElectronicoParticipante'];

		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';


		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Pontificia Universidad Catolica del Ecuador');
		$this->email->subject('Confirmación de inscripción a evento: "'.$data['evento']['tituloEvento'].'"');
		$this->email->to($participante['correoElectronicoParticipante']);
		$data['emailParticipante']=$participante['correoElectronicoParticipante'];
		$mensaje=$this->load->view('emailTemplates/inscripcionParticipanteNuevo', $data, TRUE);

		$this->email->message($mensaje);
		$this->email->send();
		//echo $this->email->print_debugger();
	}

	/**
	 * Notifica a un participante previamente inscrito cuando completa el formulario correctamente
	 * @param int $idParticipanteEvento
	 */
	protected function notificarRegistro($idParticipante, $contrasena){
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($idParticipante);
		$data['contrasena']=$contrasena;
		$data['email']=$participante['correoElectronicoParticipante'];

		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';


		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Pontificia Universidad Catolica del Ecuador');
		$this->email->subject('Confirmación de registro al sistema de matrículas');
		$this->email->to($participante['correoElectronicoParticipante']);
		$data['emailParticipante']=$participante['correoElectronicoParticipante'];
		$mensaje=$this->load->view('emailTemplates/registroParticipante', $data, TRUE);

		$this->email->message($mensaje);
		$this->email->send();
		//echo $this->email->print_debugger();
	}

	/**
	 * Recuperación de contraseña
	 * @return [type] [description]
	 */
	public function recuperarContrasena(){
		$this->load->library('form_validation');
		$this->session->set_flashdata('idEvento', $this->session->flashdata('idEvento'));

		$data['titulo']="Recuperar contraseña";

		$this->load->database();

		$this->form_validation->set_rules('email', 'Correo electrónico', 'required',
		array('required' => 'El campo "%s" debe estar lleno.'));

		if ($this->form_validation->run() == FALSE)
		{

			$data['verificacion']= $this->input->post('verificacion');

			$this->load->view('templates/header', $data);
			$this->load->view('participante/recuperacionContrasena');
			$this->load->view('templates/footer');
		}else{
			$participante=$this->participante_model->obtenerParticipantePorCorreo($this->input->post('email'));
			if($participante!=null){
				$this->enviarContrasenaNueva($participante['idParticipante']);
				$this->load->view('templates/header', $data);
				$this->load->view('participante/recuperacionExitosa');
				$this->load->view('templates/footer');
			}else{
				$this->load->view('templates/header', $data);
				$this->load->view('participante/recuperacionFallida');
				$this->load->view('templates/footer');
			}
		}
	}


	/**
	 * Genera una contraseña de 8 caracteres, la almacena en la BDD y la envía al participante
	 * @param int $idParticipante
	 */
	protected function enviarContrasenaNueva($idParticipante){
		$participante=$data['participante']=$this->participante_model->obtenerParticipante($idParticipante);
		$data['contrasenaNueva']=$rand = substr(md5(microtime()),rand(0,26),8);
		echo $data['contrasenaNueva'];
		$data['email']=$participante['correoElectronicoParticipante'];

		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';


		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('info@ccjpv.com', 'Pontificia Universidad Catolica del Ecuador');
		$this->email->subject('Generación de contraseña nueva');
		$this->email->to($participante['correoElectronicoParticipante']);
		$data['emailParticipante']=$participante['correoElectronicoParticipante'];
		$mensaje=$this->load->view('emailTemplates/contrasenaNueva', $data, TRUE);

		$this->email->message($mensaje);
		$this->email->send();

		$contrasena=md5($data['contrasenaNueva']);
		$this->participante_model->actualizarContrasena($idParticipante,$contrasena);
		//echo $this->email->print_debugger();*/
	}

	/**
	 * Devuelve un valor entero (0 o 1) si un correo existe o no
	 */
	public function verificarEmail($email){
		$email=urldecode($email);
		//echo $email;die();
		$this->output->set_content_type('application/json')->set_output(json_encode($this->participante_model->verificarEmail($email)));
	}

	/**
	 * Destuye sesión
	 */
	public function logout(){
		$this->session->sess_destroy();
		redirect('/', 'refresh');
	}
}
