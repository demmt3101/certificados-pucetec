<?php

if ( ! function_exists('obtieneImagen')){
    function obtieneImagen($archivoDoc=NULL){
		$imagen=NULL;
		if ($archivoDoc){
			$archivoPdf =  docAPdf($archivoDoc);
			if ($archivoPdf){
				$ds=DIRECTORY_SEPARATOR;
				$partesArchivo=explode(".",$archivoPdf);
				$archivoJpg=FCPATH.'application'.$ds.'cargasDocumentos'.$ds.'plantillasCertificado'.$ds.'PDF'.$ds.$partesArchivo[0].".jpeg";
				if (!file_exists($archivoJpg)){
					$archivoPdf=FCPATH.'application'.$ds.'cargasDocumentos'.$ds.'plantillasCertificado'.$ds.'PDF'.$ds.$archivoPdf;
					if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
						$comando='"c:'.$ds.'ImageMagick'.$ds.'convert"'." -density 600 ".$archivoPdf."[0] -resize 600x434! -background white -alpha remove -quality 100 ".$archivoJpg;
					else 
						$comando='convert'." -density 600 ".$archivoPdf."[0] -resize 600x434! -background white -alpha remove -quality 100 ".$archivoJpg;
					$salida =shell_exec($comando);

				}
			}			
		}
		if(isset($archivoJpg))
			return basename($archivoJpg);
		else
			return false;
    }	
}
if ( ! function_exists('docAPdf')){
    function docAPdf($archivoDoc=NULL){
		$archivoPdf=NULL;
		if($archivoDoc){
			$ds=DIRECTORY_SEPARATOR;
			$partesArchivo=explode('.',$archivoDoc);
			$destino=FCPATH.'application'.$ds.'cargasDocumentos'.$ds.'plantillasCertificado'.$ds.'PDF';
			$archivoPdf=$destino.$ds.$partesArchivo[0].'.pdf';
			if (!file_exists($archivoPdf)){
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
					$comando='"c:'.$ds.'Program Files (x86)'.$ds.'LibreOffice 5'.$ds.'program'.$ds.'soffice"';
				else
					$comando='export HOME=/tmp && soffice ';
				$fuente=FCPATH.'application'.$ds.'cargasDocumentos'.$ds.'plantillasCertificado'.$ds.$archivoDoc;
				$comando.=" --headless --convert-to pdf:writer_pdf_Export --outdir ".$destino." ".$fuente;
				$salida =shell_exec($comando);
				if (strpos($salida, 'writer_pdf_Export')!= true)
					$archivoPdf=NULL;
			}
		}
		return basename($archivoPdf);
    }
}

if ( ! function_exists('restarfechas')){
    function restarFechas($fecha1,$fecha2){
		$datetime1 = new DateTime($fecha1);
		$datetime2 = new DateTime($fecha2);
		$interval = $datetime1->diff($datetime2);
		return $interval->format('%R%a');
    }
}