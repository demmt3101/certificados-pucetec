<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    //application/vendor/phpoffice/phpword/src/PhpWord/Autoloader.php
    require_once  '../vendor/phpoffice/phpword/Autoloader.php';
    use PhpOffice\PhpWord\Autoloader as Autoloader;
    Autoloader::register();

    class Word extends Autoloader {
    }
?>
