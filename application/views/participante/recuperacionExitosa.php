<div class="ui segment container" style="min-height: 80%">
	<span style="float:right;">
		<a href="<?php echo base_url('/evento/'.$this->session->flashdata('idEvento'))?>"><i class="circular inverted large reply icon"></i> Regresar</a>
	</span>
	<br><br><br><br><br><br><br>
	<h1 class="ui teal centered dividng header"><i class="big teal mail icon"></i>Contraseña Nueva generada</h1><br>
	<p style="text-align: center">Hemos enviado su nueva contraseña a su correo electrónico. Por favor verifique el envio para poder ingresar al sistema</p>
</div>
