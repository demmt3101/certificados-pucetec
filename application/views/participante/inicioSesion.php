<div>
  <br><br><br><br>
  <div class="ui three column stackable grid">
    <div class="column">
      &nbsp;
    </div>
    <div class="center aligned column">
    <h3 class="ui teal center aligned dividing header content">Inicio de sesión</h3>
    <form action="<?php echo base_url('participante/loginParticipante')?>" method="post" class="ui large form">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="Usuario">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="contrasena" placeholder="Contraseña">
          </div>
        </div>
        <input type="submit" value="Ingresar" class="ui primary button">
      </div>
    </form>
      <?php if (isset($error)){?>
      <div class="ui error message"><?php echo $error;?></div>
      <?php }?>
      <div>
            <a class="ui teal basic button" href="<?php echo base_url('participante/registro');?>">Registrarse</a>
      </div>
      <div><br />
        <a href="<?php echo base_url('/participante/recuperarContrasena')?>" class="ui yellow sub header">Recuperar Contraseña</a>
      </div>
    </div>
    <div class="column">
      &nbsp;
    </div>
  </div>
  <br><br><br><br>
</div>
