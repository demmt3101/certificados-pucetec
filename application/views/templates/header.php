	<html>
	<head>
		<meta charset="UTF-8">
		<link rel="icon" href="<?php echo base_url("application/assets/img/favicon.ico.png")?>" type="image/png">
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/jquery-2.1.3.min.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/semantic.css') ?>">
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<script src="<?php echo base_url('application/assets/semantic-ui/semantic.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/functions.js') ?>"></script>
		<link rel="stylesheet/less" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/src/definitions/collections/menu.less') ?>">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/dataTables.jqueryui.min.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.jqueryui.min.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/scroller/1.3.0/css/scroller.jqueryui.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/dataTables.jqueryui.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/scroller/1.3.0/js/dataTables.scroller.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/moment.js') ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/moment-with-locales.js') ?>"></script>
		<script src="<?php echo base_url('application/assets/ckeditor/ckeditor.js') ?>"></script>
		<link rel="stylesheet" href="<?php echo base_url('application/assets/custom.css') ?>" type="text/css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/assets/videojs/video-js.min.css') ?>">
		<script rel="text/javascript" type="text/css" href="<?php echo base_url('application/assets/videojs/video.min.js') ?>"></script>
		<title><?php if(isset($titulo)) echo $titulo; else echo "Sistema de Matrículas CCJPV";?></title>
		<?php if (isset($evento)): ?>
			<meta property="og:title" content="<?php echo $evento['tituloEvento'] ?>" />
			<meta name="description" content="<?php echo strip_tags($evento['descripcionEvento']) ?>">
	    <meta property="og:description" content="<?php echo strip_tags($evento['descripcionEvento']) ?>" />
	    <meta property="og:image" content="<?php echo base_url('application/cargasDocumentos/imagenes/'.$evento['imagenEvento']); ?>" />

			<meta name="twitter:card" content="summary_large_image">
			<meta name="twitter:site" content="@ccjpv_ec">
			<meta name="twitter:creator" content="@ccjpv_ec">
			<meta name="twitter:title" content="<?php echo $evento['tituloEvento']; ?>" />
			<meta name="twitter:description" content="<?php echo strip_tags($evento['descripcionEvento']) ?>">
	    <meta name="twitter:image" content="<?php echo base_url('application/cargasDocumentos/imagenes/'.$evento['imagenEvento']); ?>" />

		<?php else: ?>
			<meta property="og:title" content="<?php if(isset($titulo)) echo $titulo; else echo "Sistema de Matrículas CCJPV";?>" />
			<meta name="description" content="<?php if(isset($titulo)) echo $titulo; else echo "Sistema de Matrículas CCJPV";?>">
			<meta property="og:description" content="<?php if(isset($titulo)) echo $titulo; else echo "Sistema de Matrículas CCJPV";?>" />
	    <meta property="og:image" content="<?php echo base_url('application/assets/images/ccjpv-teal.svg')?>" />

			<meta name="twitter:card" content="summary" />
		 	<meta name="twitter:site" content="@ccjpv_ec" />
			<meta name="twitter:title" content="<?php if(isset($titulo)) echo $titulo; else echo "Sistema de Matrículas CCJPV";?>" />
			<meta property="twitter:image" content="<?php echo base_url('application/assets/images/ccjpv-teal.svg')?>" />
			<meta name="twitter:author" content="@ccjpv_ec">
		<?php endif; ?>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<header>
			<div class="ui teal inverted segment">
				<br />
				<div class="ui container" style="border-radius: 0px !important;">
					<div class="ui stackable grid">
						<div class="sixteen wide column">
							<a href="<?php echo base_url(); ?>">
								<img src="<?php echo base_url('application/assets/images/ccjpv-white.svg')?>" class="ui large centered image">
							</a>
						</div>
						<div class="sixteen wide right aligned column">
							<p class="ui inverted sub header">Sistema Administrador de Matrículas</p>
						</div>
						<div class="four wide column">
							<a href="http://ccjpv.com" class="ui inverted basic vertical animated button" tabindex="0">
							  <div class="hidden content"><i class="left arrow icon"></i></div>
							  <div class="visible content">
							    Volver a CCJPV
							  </div>
							</a>
						</div>
						<div class="three wide column">
							&nbsp
						</div>
						<div class="three wide right aligned middle aligned column">
							<?php if($this->session->userdata('nombresParticipante')!=null):?>
								<i class="user big inverted icon"></i>
							<?php else:?>
								&nbsp;
							<?php endif;?>
						</div>
						<div class="three wide column middle aligned right aligned">
							<?php if($this->session->userdata('nombresParticipante')!=null):?>
								<div class="ui inverted sub header">Bienvenid@:
								<br><?php echo $this->session->userdata('nombresParticipante').' '.$this->session->userdata('apellidosParticipante')?></div>
							<?php else:?>
								<a href="<?php echo base_url()?>/participante/registro" class="ui inverted basic button">Registro</a>
							<?php endif;?>
						</div>
						<div class="three wide column middle aligned right aligned">
							<?php if($this->session->userdata('nombresParticipante')!=null):?>
								<a class="ui inverted button" href="<?php echo base_url()?>/participante/logout">Salir</a>
							<?php else:?>
								<a class="ui inverted basic button" href="<?php echo base_url()?>/participante/loginParticipante">Iniciar Sesi&oacute;n</a>
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
		</header>
