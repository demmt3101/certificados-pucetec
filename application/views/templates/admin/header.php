	<html>
	<head>
		<meta charset="UTF-8">
		<link rel="icon" href="<?php echo base_url("application/assets/img/favicon.ico.png")?>" type="image/png">
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/jquery-2.1.3.min.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/semantic.css') ?>">
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<script src="<?php echo base_url('application/assets/semantic-ui/semantic.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/functions.js') ?>"></script>
		<link rel="stylesheet/less" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/src/definitions/collections/menu.less') ?>">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/dataTables.jqueryui.min.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.jqueryui.min.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/scroller/1.3.0/css/scroller.jqueryui.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/dataTables.jqueryui.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/scroller/1.3.0/js/dataTables.scroller.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/moment.js') ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/moment-with-locales.js') ?>"></script>
		<script src="<?php echo base_url('application/assets/ckeditor/ckeditor.js') ?>"></script>
		<link rel="stylesheet" href="<?php echo base_url('application/assets/custom.css') ?>" type="text/css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/assets/videojs/video-js.min.css') ?>">
		<script rel="text/javascript" type="text/css" href="<?php echo base_url('application/assets/videojs/video.min.js') ?>"></script>
		<title><?php echo $titulo?></title>
	</head>
	<body>
		<header>
			<div class="ui teal inverted segment">
				<div class="ui container" style="border-radius: 0px;">
					<div class="ui stackable grid">
						<div class="four wide column">
							<img src="<?php echo base_url('application/assets/images/ccjpv-white.png')?>" class="ui fluid image">
						</div>
						<div class="six wide centered middle aligned column">
							<h3 class="ui inverted header">Administrador de Matrículas</h3>
						</div>
						<div class="two wide right aligned middle aligned column">
							<?php if($this->session->userdata('nombresEmpleado')!=null):?>
								<i class="user big inverted icon"></i>
							<?php else:?>
								&nbsp;
							<?php endif;?>
						</div>
						<div class="two wide column middle aligned">
							<?php if($this->session->userdata('nombresEmpleado')!=null):?>
								<div class="ui inverted sub header">Bienvenid@:
								<br><?php echo $this->session->userdata('nombresEmpleado').' '.$this->session->userdata('apellidosEmpleado')?></div>
							<?php else:?>
								&nbsp;
							<?php endif;?>
						</div>
						<div class="two wide column middle aligned">
							<?php if($this->session->userdata('nombresEmpleado')!=null):?>
								<a class="ui inverted button" href="<?php echo base_url()?>admin/empleado/logout">Salir</a>
							<?php else:?>
								&nbsp;
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
		</header>
