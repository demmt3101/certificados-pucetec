<html>
	<body>
		<div class="ui text container">
			<div class="ui segment">
				<h3 class="ui blue header">Estimad@ <?php echo $participante['nombresParticipante']?> <?php echo $participante['apellidosParticipante']?>: </h3>
				<p>Lamentamos informar que el evento "<strong><?php echo $evento['tituloEvento']?></strong>" ha sido cancelado, debido a las siguientes observaciones:</p>
				<pre><?php echo ['observacionesModificacion']?></pre>
				<p>Fecha de inicio: <strong><?php echo $evento['fechaInicioEvento']?></strong></p>
				<p>Fecha de fin: <strong><?php echo $evento['fechaFinEvento']?></strong></p>
				<p>En caso que usted no se haya matriculado en este evento, por favor notifique a <a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
				para eliminar su inscripción.</p>
				<p>Gracias por formar parte del Centro de Capacitación José Pedro Varela</p>
			</div>
			<div style="text-align: right; font-size: 9pt">
				<img width="150px" src="<?php echo base_url('application/assets/images/ccjpv-teal.png')?>"/><br>
				Tomás de Berlanga E10-115 e Isla Pinzón (esquina)<br>
				Quito, Ecuador.<br>
				Telfs: (593 2) 2453-585<br>
				<a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
			</div>
		</div>
	</body>
</html>
