<html>
	<body>
		<div class="ui text container">
			<div class="ui segment">
				<h3 class="ui blue header">Estimad@ <?php echo $participante['nombresParticipante']?> <?php echo $participante['apellidosParticipante']?>: </h3>
				<p>Hemos recuperado sus reestablecido su ingreso al sistema de matrículas.
				Desde ahora deberá ingresar con las siguientes credenciales:</p>
				<ul>
					<li>Nombre de Usuario: <strong><?php echo $email?></strong></li>
					<li>Contraseña: <strong><?php echo $contrasenaNueva?></strong></li>
				</ul>
				<p>En caso de que usted no haya solicitado recuperar su contraseña, por favor comunicarse a <a href="mailto:info@ccjpv.com">info@ccjpv.com</a>.</p>
				<p><strong>IMPORTANTE:</strong> Le recomendamos conservar este correo para evitar futuras pédidas de contraseñas</p>
			</div>
			<div style="text-align: right; font-size: 9pt">
				<img width="150px" src="<?php echo base_url('application/assets/images/ccjpv-teal.png')?>"/><br>
				Tomás de Berlanga E10-115 e Isla Pinzón (esquina)<br>
				Quito, Ecuador.<br>
				Telfs: (593 2) 2453-585<br>
				<a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
			</div>
		</div>
	</body>
</html>
