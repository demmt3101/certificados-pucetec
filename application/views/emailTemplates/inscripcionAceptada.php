<html>
	<body>
		<div class="ui text container">
			<div class="ui segment">
				<h3 class="ui blue header">Estimad@ <?php echo $participante['nombresParticipante']?> <?php echo $participante['apellidosParticipante']?>: </h3>
				<p>Confirmamos tu inscripción y la recepción del pago para el evento:
				"<strong><?php echo $evento['tituloEvento']?></strong>"</p>
				<p>Estamos en la elaboración de tu factura, la misma que saldrá con los datos que ingresaste previamente en el sistema.</p>
				<p>Te recordamos que el evento se realizará el: <strong><?php echo $evento['fechaInicioEvento']?></strong> hasta: <strong><?php echo $evento['fechaFinEvento']?></strong></p>
				<p>Te esperamos en CIESPAL. </p>
				<p>Observaciones: "<?php echo $participanteEvento['observacionesParticipanteEvento']?>"</p>
				<p>En caso que usted no se haya matriculado en este evento, por favor notifique a <a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
				para eliminar su inscripción.</p>
			</div>
			<div style="text-align: right; font-size: 9pt">
				<img width="150px" src="<?php echo base_url('application/assets/images/ccjpv-teal.png')?>"/><br>
				Tomás de Berlanga E10-115 e Isla Pinzón (esquina)<br>
				Quito, Ecuador.<br>
				Telfs: (593 2) 2453-585<br>
				<a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
			</div>
		</div>
	</body>
</html>
