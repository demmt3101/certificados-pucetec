<html>
	<body>
		<div class="ui text container">
			<div class="ui segment">
				<h3 class="ui blue header">Estimad@ <?php echo $participante['nombresParticipante']?> <?php echo $participante['apellidosParticipante']?>: </h3>
				<p>Gracias por registrarse en el sistema de matrículas de CIESPAL. Desde este momento usted podrá registrarse a todos
				los eventos de CIESPAL con las siguientes credenciales:</p>
				<ul>
					<li>Nombre de Usuario: <strong><?php echo $email?></strong></li>
					<li>Contraseña: <strong><?php echo $contrasena?></strong></li>
				</ul>
				<p>Usted se ha inscrito en el evento: "<strong><?php echo $evento['tituloEvento']?></strong>"</p>
				<p>Fecha de inicio: <strong><?php echo $evento['fechaInicioEvento']?></strong></p>
				<p>Fecha de fin: <strong><?php echo $evento['fechaFinEvento']?></strong></p>
				<p>Le recordamos que (en el caso de eventos pagados) su pago lo puede realizar
				mediante transferencia electrónica a la
				cuenta corriente Produbanco # 2102006292 a nombre de CIESPAL, Ruc: 1791719913001.
				Una vez realizado el pago, enviar el comprobante a:
				<a href="info@ccjpv.com">info@ccjpv.com</a> y
				<a href="info@ccjpv.com">info@ccjpv.com</a>.</p>
				<p>Si requiere solventar alguna inquietud, por favor comuníquese a los teléfonos: (5932) 2548011, 2567966, extensiones 139 y 153.</p>
				<p>En caso que usted no se haya matriculado en este evento, por favor notifique a <a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
				para eliminar su inscripción.</p>
				<p><strong>IMPORTANTE:</strong> Conserve este correo para evitar pédidas de contraseñas</p>
			</div>
			<div style="text-align: right; font-size: 9pt">
				<img width="150px" src="<?php echo base_url('application/assets/images/ccjpv-teal.png')?>"/><br>
				Tomás de Berlanga E10-115 e Isla Pinzón (esquina)<br>
				Quito, Ecuador.<br>
				Telfs: (593 2) 2453-585<br>
				<a href="mailto:info@ccjpv.com">info@ccjpv.com</a>
			</div>
		</div>
	</body>
</html>
