<br>
<div class="ui container segment">

	<h1 class="ui teal dividing header"><?php echo $evento['tituloEvento']?></h1>
	<div class="ui two column grid">
		<div class="column">
			<h3 class="ui teal header">Fechas</h3>
			<p><strong>Fecha y Hora de Inicio: </strong><?php echo $evento['fechaInicioEvento']?><br></p>
			<p><strong>Fecha y Hora de Finalización: </strong><?php echo $evento['fechaFinEvento']?></p><br><br>
			<p><strong>Emisión certificado: </strong><?php if ($evento['certificadoEvento']==0) echo 'No'; else echo 'Sí';?></p>
		</div>
		<div class="column">
			&nbsp;
		</div>
	</div>

	<form action="" class="ui form" method="post" enctype="multipart/form-data">
		<input type="hidden" id="idEvento" name="idEvento" value="<?php echo $this->session->userdata('idEvento');?>">
		<div class="ui grid">
			<div class="one column row">
				<div class="column"><h3 class="ui teal dividing header">Datos de participante</h3></div>
			</div>
			<div class="two column row">
				<div class="column">
					<strong>Apellidos: </strong><?php echo $participante['apellidosParticipante']?>
				</div>
				<div class="column">
					<strong>Nombres: </strong><?php echo $participante['nombresParticipante']?>
				</div>
				<div class="column">
					<strong><?php echo $descripcionTipoIdentificacion['descripcionTipoIdentificacion'];?>: </strong><?php echo $participante['identificacionParticipante']?>
				</div>
				<div class="column">
					<strong>Email: </strong><?php echo $participante['correoElectronicoParticipante']?>
				</div>
			</div>
			<?php if (!$this->evento_model->esGratuito($this->session->userdata('idEvento'))):?>
			<div class="one column row">
				<div class="column"><br><br><h3 class="ui dividing teal header">Datos de facturación</h3></div>
			</div>

				<div class="three column row" id="facturas">
				<?php
				$i=0;
				foreach ($datosFacturacion as $datoFacturacion):?>
					<div class="column">
						<div class="ui segment" id="factura<?php echo $datoFacturacion['idDatosFacturacion']?>">
							<div class="field" id="idDatosFacturacion">
								<span style="float:left">
								      <div class="ui checkbox" style="border: 1px solid #002A45; padding: 0px; min-height: 20px; padding-left: 0.1em;padding-right: 0.1em; text-align: center;">
								        <input type="radio" name="idDatosFacturacion" value="<?php echo $datoFacturacion['idDatosFacturacion']?>">
								        <label></label>
								      </div>
								</span>
								<span style="float:right;">
									<i class="edit icon"></i>
								</span>
								<h5 class="ui teal dividing centered sub header">Factura <?php echo ++$i;?></h5><br>
							</div>
							<p><strong>RUC: </strong><?php echo $datoFacturacion['rucDatosFacturacion']?></p>
							<p><strong>Razón Social: </strong><?php echo $datoFacturacion['razonSocialDatosFacturacion']?></p>
							<p><strong>Dirección: </strong><?php echo $datoFacturacion['direccionDatosFacturacion']?></p>
							<p><strong>Teléfono: </strong><?php echo $datoFacturacion['telefonoDatosFacturacion']?></p>
						</div>
					</div>
				<?php endforeach;?>
					<div class="column">
						<div class="ui segment" id="facturaNueva">
							<div class="field" id="idDatosFacturacion">
								<span style="float:left">
								      <div class="ui checkbox" style="border: 1px solid #002A45; padding: 0px; min-height: 20px; padding-left: 0.1em;padding-right: 0.1em; text-align: center;">
								        <input type="radio" name="idDatosFacturacion" value="Nueva">
								        <label></label>
								      </div>
								</span>
								<span style="float:right;">
									<i class="edit icon"></i>
								</span>
								<h5 class="ui teal dividing centered sub header">Nueva Factura</h5><br>
							</div>
							<div class="field" id="ruc">
								<label>RUC: </label>
								<input name="rucDatosFacturacion" id="rucDatosFacturacion" type="text">
							</div>
							<div class="field" id="razonSocial">
								<label>Razón Social: </label>
								<input name="razonSocialDatosFacturacion" id="razonSocialDatosFacturacion" type="text">
							</div>
							<div class="field" id="direccion">
								<field>Dirección: </field>
								<input name="direccionDatosFacturacion" id="direccionDatosFacturacion" type="text">
							</div>
							<div class="field" id="telefono">
								<strong>Teléfono: </strong>
								<input name="telefonoDatosFacturacion" id="telefonoDatosFacturacion" type="text">
							</div>
						</div>
					</div>
				</div>
			<?php endif;?>
			<div class="one column row">
				<div class="column"><h3 class="ui dividing teal header">Datos de Matrícula</h3></div>
			</div>
			<div class="two column row">
				<div class="column">
					<div class="field">
	      				<label>Usted participa como:</label>
						<select id="idTipoParticipante" name="idTipoParticipante" class="ui search dropdown">
							<?php foreach ($costos as $costo):?>
								<option value="<?php echo $costo['idTipoParticipante']?>"><?php echo $costo['descripcionTipoParticipante']?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Costo:</label>
						<input type="text" id="despliegueCostos" name="valorAPagar" value="<?php echo number_format($costos[0]['costoValorEvento'], 2)?>" readonly>
					</div>
				</div>
				<?php if (!$this->evento_model->esGratuito($this->session->userdata('idEvento'))):?>
				<div class="column">
					<div class="field">
						<label>Forma de pago:</label>
						<select id="formaPago" name="idTipoFormaPago"  class="ui search dropdown">
							<?php foreach ($formasPago as $formaPago):?>
								<option value="<?php echo $formaPago['idTipoFormaPago']?>"><?php echo $formaPago['descripcionTipoFormaPago']?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="column">
					<div class="field" id="comprobante" style="display:none">
						<label>Comprobante de depósito:</label>
						<input type="file" id="comprobantePago" name="comprobantePago">
					</div>
					<div id="mensajePagoDeposito" class="ui info message" style="display:none">
					  	<i class="close icon" onclick="$('.ui.info.message').transition('slide')"></i>
					  	<div class="header">
					    	Pago mediante transferencia:
					  	</div>
							<div class="content">
								Se debe depositar a la cuenta de <br /><strong>Ahorros de Banco Pichincha # 3291569200</strong><br />a nombre de
								<strong>Inés Martínez Moreno</strong>,<br />CI/RUC: <strong>0601273287</strong>.<br><br>
								Una vez realizado el depósito deberá agregar el depósito en el campo correspondiente
							</div>
					 </div>
					<div id="mensajePagoEfectivo" class="ui info message" style="display:none">
					  	<i class="close icon" onclick="$('.ui.info.message').transition('slide')"></i>
					  	<div class="header">
					    	Pago en efectivo:
					  	</div>
							<div class="content">
								Un representante del Centro de Capacitación José Pedro Varela se comunicará con usted para
								confirmar su asistencia al evento, usted podrá pagar hasta el día del inicion del mismo.
							</div>
					 </div>
				</div>
				<?php endif;?>
			</div>
			<div class="one column row">
				<?php if (!$this->evento_model->esGratuito($this->session->userdata('idEvento'))):?>
				<div class="column">
					<div class="field">
						<label>Detalles de comprobante de pago:</label>
						<textarea id="descripcionEvidenciaPago" name="descripcionEvidenciaPago"></textarea>
					</div>
				</div>
				<?php endif;?>
				<?php if ($evento['certificadoEvento']!=0):?>
				<div class="column">
					<div class="field">
						<label>Nombre del Certificado:</label>
						<input type="text" id="certificadoParticipanteEvento" name="nombreCertificado" value="<?php echo $participante['nombresParticipante'].' '.mb_strtoupper($participante['apellidosParticipante']);?>">
					</div>
				</div>
				<?php endif;?>
				<div class="center aligned column"><br>
					<input type="submit" class="ui teal button" value="Grabar">
					<div class="ui cancel button">Vaciar Formulario</div>
				</div>
			</div>

		</div>

	</form>
</div>
<script type="text/javascript">
<!--

//-->

var costos = <?php echo json_encode($costos); ?>;

$('.ui.checkbox').checkbox({
	onChecked: function() {
		$('h5').removeClass('inverted');
		//$('.ui.segment').removeClass('teal inverted');
		$('h5').addClass('teal');
		$('#facturas .ui.segment').removeClass('teal inverted');
		$('#factura'+this.value).addClass('teal inverted');
		$('#factura'+this.value +' h5 ').removeClass('teal');
		$('#factura'+this.value +' h5 ').addClass('inverted');
	},
});

$('#idTipoParticipante')
.dropdown({
	onChange: function(value, text, $selectedItem) {
	      $.each(costos, function(i, costo){
		      valor=parseFloat(costo.costoValorEvento);
		      if (costo.idTipoParticipante==value)
		    	  $('#despliegueCostos').val(''+valor.toFixed(2));
	    });
	}
});

$('#formaPago').dropdown({
  onChange: function(value, text, $selectedItem) {
		console.log('entra');
		if (value == 2) {
				$('#mensajePagoEfectivo').hide();
				$('#comprobante').show();
				$('#mensajePagoDeposito').show();
					$("#comprobantePago").prop('disabled', false);
		} else if (value == 3) {
				$('#comprobante').hide();
				$('#mensajePagoDeposito').hide();
				$('#mensajePagoEfectivo').show();
				$("#comprobantePago").prop('disabled', true);
		}
  }
});

var formulario
$( document ).ready(function() {

$.fn.form.settings.rules.countFacturacion= function(value) {
	if($('.ui.checkbox.checked').length > 0) return true;
	return false;
}

$.fn.form.settings.rules.ruc= function(value) {
	if($('.ui.checkbox.checked>input').val()=='Nueva' && $('#rucDatosFacturacion').val()==''){
				return false;
		}
	return true;
}

$.fn.form.settings.rules.razonSocial= function(value) {
	if($('.ui.checkbox.checked>input').val()=='Nueva' && $('#razonSocialDatosFacturacion').val()==''){
				return false;
		}
	return true;
}

$.fn.form.settings.rules.direccion= function(value) {
	if($('.ui.checkbox.checked>input').val()=='Nueva' && $('#direccionDatosFacturacion').val()==''){
				return false;
		}
	return true;
}
$.fn.form.settings.rules.validacionComprobantePago= function(value) {
	extension=value.substr(value.length - 4);
	if(value=='' || extension==".gif" || extension==".jpg" || extension=="jpeg" || extension==".png")
		return true;
	return false;
}

$.fn.form.settings.rules.telefono= function(value) {
	if($('.ui.checkbox.checked>input').val()=='Nueva' && $('#telefonoDatosFacturacion').val()=='')
			return false;

	if ($('.ui.checkbox.checked>input').val()=='Nueva' && !$.isNumeric($('#telefonoDatosFacturacion').val()))
			return false;
	return true;
}

formulario=$('.ui.form')
 .form({
	  fields: {
		idDatosFacturacion: {
	      identifier: 'idDatosFacturacion',
	      rules: [
	        {
	          type   : 'countFacturacion',
	          prompt : 'Por favor seleccione almenos un tipo de facturación'
	        }
	      ]
	    },
	    ruc: {
		      identifier: 'rucDatosFacturacion',
		      rules: [
		        {
		          type   : 'ruc',
		          prompt : 'Por favor debe ingresar un ruc'
		        }
		      ]
		  },
		razonSocial: {
		      identifier: 'razonSocialDatosFacturacion',
		      rules: [
		        {
		          type   : 'razonSocial',
		          prompt : 'Por favor debe ingresar una Razón Social'
		        }
		      ]
		  },

		direccion: {
		      identifier: 'direccionDatosFacturacion',
		      rules: [
		        {
		          type   : 'direccion',
		          prompt : 'Por favor debe ingresar una Dirección Válida'
		        }
		      ]
		  },
		telefono: {
		      identifier: 'telefonoDatosFacturacion',
		      rules: [
		        {
		          type   : 'telefono',
		          prompt : 'Por favor debe ingresar un valor numérico para almacenar su teléfono'
		        }
		      ]
		  },
		comprobante: {
		      identifier: 'comprobantePago',
		      rules: [
						{
							type   : 'empty',
             	prompt : 'Debe agregar comprobante de depósito'
		        },
						{
		          type   : 'validacionComprobantePago',
		          prompt : 'Solo puede ingresar el comprobante en formato de imagen GIF, JPG o PNG'
		        }
		      ]
		  }
	  },
	  inline: true
	});
});

function comprobarFormaPago() {
	if ($('#formaPago').dropdown('get value') == '2') {
			//pago por depósito
			$('#comprobante').show();
			$('#mensajePagoDeposito').show();
			$("#comprobantePago").prop('disabled', false);
	} else if ($('#formaPago').dropdown('get value') == '3') {
			//Pago en efectivo
			$('#mensajePagoEfectivo').show();
			$("#comprobantePago").prop('disabled', true);
	}
}

comprobarFormaPago();

</script>
