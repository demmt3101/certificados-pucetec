<div class="ui container">
	<?php if (isset($descuentoRegistrado) && $descuentoRegistrado!=null):?>
		<div class="ui segment">
			<h3 class="ui dividing header">Descuentos</h3>
			<div class="ui teal sub header">Tipo de descuento solicitado: <?php echo $descuentoRegistrado['descripcionDescuento']?></div>
				<?php if ($participante['documentoDescuento'] != null): ?>
					<img src="<?php echo base_url('/application/cargasDocumentos/comprobantes/descuentos/'.$participante['documentoDescuento'])?>" class="ui big centered image"/><br>
				<?php endif; ?>
			<div style="text-align: center;">
				<div class="ui red inverted button" onclick="$('#modalRechazoDescuento').modal('show')">
					<i class="large remove icon"></i> Rechazar descuento
				</div>
			</div><br>
		</div>
	<?php endif;?>
	<div class="ui segment">
		<h3 class="ui teal dividing header">Para uso de CCJPV</h3>
		<?php if ($datosFacturacion['rutaEvidenciaPago']!=''):?>
		<div class="ui teal sub header">Comprobante de pago</div><br>
		<img src="<?php echo base_url('/application/cargasDocumentos/comprobantes/'.$datosFacturacion['rutaEvidenciaPago'])?>" class="ui big centered image"/><br>
		<?php endif;?>
		<form action="<?php echo base_url('/admin/participante/actualizarDatosFinancieros/'.$datosFacturacion['idParticipanteEvento'])?>" method="post" class="ui form">
			<div class="ui two column stackable grid">
				<div class="column">
					<div class="field">
						<label>No. de Factura</label>
						<input name="facturaCiespalParticipanteEvento" type="text" id="numeroFactura">
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Valor Cancelado</label>
						<input type="number" name="valorPagadoParticipanteEvento" value="<?php echo number_format($datosFacturacion['valorAPagar'], 2)?>" step="any">
					</div>
				</div>
				<div class="column">
					<input name="confirmadoPorParticipanteEvento" type="hidden" value="<?php echo $this->session->userdata('idEmpleado')?>">
					<div class="field">
						<label>Estado de Inscripción</label>
						<div class="ui selection dropdown">
						  <input type="hidden" name="estadoParticipanteEvento" value="1">
						  <i class="dropdown icon"></i>
						  <div class="default text">Estado de Inscripción</div>
						  <div class="menu">
						    <div class="item" data-value="1">Aceptada</div>
						    <div class="item" data-value="2">Rechazada</div>
						  </div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Observaciones</label>
						<textarea name="observacionesParticipanteEvento" id="observacionesParticipanteEvento" rows="4" cols="70"></textarea>
					</div>
				</div>
			</div>
			<div style="text-align: center"><br>
				<input type="submit" class="ui teal button">
				<a href="#" onclick="$('#modalEliminacion').modal('show')" class="ui red button">Eliminar Participante</a>
			</div>
		</form>
	</div>
</div>
<div class="ui basic modal" id="modalEliminacion">
  <i class="close icon"></i>
  <div class="header">
    Eliminar Participante
  </div>
  <div class="image content">
    <div class="image">
      <i class="trash icon"></i>
    </div>
    <div class="description">
      <p>Usted está a punto de eliminar a <?php echo $participante['nombresParticipante'] ?> <?php echo $participante['apellidosParticipante']?>
        de este evento. Está seguro que desea realizar esta acción?
      </p>
    </div>
  </div>
  <div class="actions">
    <div class="ui two buttons">
      <div class="ui cancel red basic button" onclick="$('#modalEliminacion').modal('hide')">
        <i class="remove icon"></i>
        No
      </div>
      <div class="ui ok green basic inverted button" onclick="eliminarParticipante()">
        <i class="checkmark icon"></i>
        Sí
      </div>
    </div>
  </div>
</div>

<div class="ui modal" id="modalRechazoDescuento">
  <i class="close icon"></i>
  <div class="header">
    Rechazar Solicitud de Descuento
  </div>
  <div class="image content">
    <div class="image">
      <i class="delete red icon"></i>
    </div>
    <div class="description">
      <p>Usted está a unto de rechazar la solicitud de descuento de <?php echo $participante['nombresParticipante'] ?> <?php echo $participante['apellidosParticipante']?>.
      	Esta acción eliminará el descuento solicitado permanentemente para todos los eventos del sistema.
      </p>
      <label>Observaciones</label><br>
      <textarea id="observacionesDescuento" rows="10" cols="60"></textarea>
    </div>
  </div>
  <div class="actions">
    <div class="two fluid ui inverted buttons">
      <div class="ui cancel red basic button" onclick="$('#modalRechazoDescuento').modal('hide')">
        <i class="remove icon"></i>
        No
      </div>
      <div class="ui ok green basic button" onclick="rechazarDescuento()">
        <i class="checkmark icon"></i>
        Sí
      </div>
    </div>
  </div>
</div>

<script>
$('.ui.dropdown')
.dropdown();

function eliminarParticipante(){
	$('body').append('<div id="envioCorreo" class="ui active dimmer"><div class="ui large text loader"><i class="inverted mail icon"></i> Enviando Notificación a participante. Por favor espere</div></div>')
	$.ajax({
        type:"POST",
        dataType:"html",
        url: baseUrl+"admin/evento/eliminarParticipante/" + <?php echo $datosFacturacion['idParticipanteEvento']?>,
        data: { observacionesParticipanteEvento: ""+$('#observacionesParticipanteEvento').val()},
        success:function(msg){
        	window.location='<?php echo base_url();?>/admin/evento/detalles/<?php echo $datosFacturacion['idEvento'];?>';
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });
}

function rechazarDescuento(){
	$('body').append('<div id="envioCorreo" class="ui active dimmer"><div class="ui large text loader"><i class="inverted mail icon"></i> Enviando Notificación a participante. Por favor espere</div></div>')
	$.ajax({
        type:"POST",
        dataType:"html",
        url: baseUrl+"admin/participante/rechazarDescuento/" + <?php echo $datosFacturacion['idParticipanteEvento']?>,
        data: { observaciones: ""+$('#observacionesDescuento').val()},
        success:function(msg){
        	location.reload();
        },
        statusCode:{
        	500:function(){
        		console.log("hubo un error")
        	}
        }
    });
}
</script>
