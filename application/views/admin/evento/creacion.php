<link rel='stylesheet' href='<?php echo base_url('application/assets/custom/jquery.datetimepicker.css') ?>' />
<script type="text/javascript" src="<?php echo base_url('application/assets/js/jquery.datetimepicker.full.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/assets/js/evento.js') ?>"></script>
<div class="ui container margenmod">
	<span style="float:right">
		<a class="ui icon button" href="<?php echo base_url('/admin/evento/');?>">
		  <i class="reply icon"></i>
		</a>
	</span>
	<h1 class="ui teal dividing header">Crear Evento</h1>
	<form action="<?php echo base_url()?>/admin/evento/crear" method="post" class="ui form" enctype="multipart/form-data" >
		<div class="ui two column grid margenmod">
			<div class="column">
				<div class="ui segment">
					<h5 class="ui teal centered header">Datos del Evento</h5>
					<div class="field">
						<label>Tipo de Evento</label>
						<select name="tipoEvento" id="tipoEvento">
							<?php foreach ($tiposEvento as $tipoEvento):?>
								<option value="<?php echo $tipoEvento['idTipoEvento']?>"><?php echo $tipoEvento['descripcionTipoEvento']?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="field">
						<label>Título de Evento</label>
		    		<input type="text" placeholder="Curso de redaccion periodistica" name="nombreEvento" id="nombreEvento">
					</div>
					<div class="field">
						<label>Descripción de Evento</label>
		    		<textarea placeholder="Curso de redaccion periodistica" name="descripcionEvento" id="descripcionEvento"> </textarea>
						<script>
							CKEDITOR.replace( 'descripcionEvento' );
						</script>
					</div>
					<div class="field" id="fechaInicio">
						<label>Fecha de Inicio</label>
		    			<input type="text" placeholder="<?php echo date('Y/m/d h:i')?>" name="fechaInicioEvento" id="fechaInicioEvento">
					</div>
					<div class="field" id="fechaFin">
						<label>Fecha de Fin</label>
		    			<input type="text" placeholder="<?php echo date('Y/m/d h:i')?>" name="fechaFinEvento" id="fechaFinEvento">
					</div>
					<div class="field" id="cert">
						<div class="ui checkbox" id="certificadoEvento">
						  <input type="checkbox" name="certificadoEvento" id="certificadoCheckbox" onclick="requiereCertificado()">
						  <label>Se entrega certificado</label>
						</div><br>
					</div>
					<div class="field" id="documentoCertificado" style="display: none;">
						<label>Documento</label>
						<input type="file" name="plantillaEvento" id="documento" accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
					</div>
					<div class="field" id="imagen">
						<label>Imagen</label>
						<input type="file" name="imagenEvento" id="imagenEvento" accept="image/*"/>
					</div>
					<br><div class="field" id="cupo">
						<label>Cupo (0 es Ilimitado)</label>
		    			<input type="number" placeholder="Nombre" name="cupoEvento" value="0">
					</div>
				</div>
			</div>
			<div class="column">
				<div class="ui segment">
					<div class="field" id="tipoParticipante">
						<h5 class="ui teal centered header">Costos</h5>
					</div>
					<h5 class="ui dividing teal header">Tipo de Participante</h5>
					<?php foreach ($tiposParticipante as $tipoParticipante):?>
					<div class="field" id="tipoParticipante<?php echo $tipoParticipante['idTipoParticipante'];?>">
						<div class="ui checkbox">
						  <input type="checkbox" id="idTipoParticipante" name="idTipoParticipante[]" value="<?php echo $tipoParticipante['idTipoParticipante']?>"
						  			onclick="seleccionarTipoParticipante(<?php echo $tipoParticipante['idTipoParticipante'];?>)">
						  <label><?php echo $tipoParticipante['descripcionTipoParticipante']?></label>
						</div>
					</div>
					<div class="field" id="tipoParticipanteCosto<?php echo $tipoParticipante['idTipoParticipante']?>"  style="display: none">
						<label>Valor: </label>
						<input type="number" name="tipoParticipanteValor<?php echo $tipoParticipante['idTipoParticipante']?>" value="0.00" min="0" step="any"/>(Incluir IVA)
					</div>
					<?php endforeach;?>
				</div>
			</div>
		</div>
		<br>
		<div class="margenmod" style="text-align: center">
			<input type="submit" class="ui teal button" value="Guardar"/>&nbsp;&nbsp;
			<a href="<?php echo base_url('/admin/evento')?>" class="ui button">Cancelar</a>
		</div>
	</form>
</div>
<script type="text/javascript">

</script>
