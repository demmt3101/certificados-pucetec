<div class="ui container">
	<div class="ui teal segment">
		<h2 class="ui inverted dividing header">Resumen Financiero</h2>
		<div class="ui two column very relaxed grid">
			<div class="column">
				<p><strong>Monto por cobrar: </strong>$<?php echo number_format($montoPorCobrar, 2);?></p>
				<p><strong>Monto cobrado: </strong>$<?php echo number_format($montoCobrado, 2);?></p>
			</div>
			<div class="column">
				<p><strong>Diferencia: </strong><font color="red">$ <?php echo number_format($montoPorCobrar - $montoCobrado, 2);?></font></p>
			</div>
		</div>
	</div>
</div>
