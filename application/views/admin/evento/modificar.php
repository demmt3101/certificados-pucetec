<link rel='stylesheet' href='<?php echo base_url('application/assets/custom/jquery.datetimepicker.css') ?>' />
<script type="text/javascript" src="<?php echo base_url('application/assets/js/jquery.datetimepicker.full.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/assets/js/modificacionEvento.js') ?>"></script>
<div class="ui container margenmod">
	<span style="float:right">
		<a class="ui icon button" href="<?php echo base_url('/admin/evento/');?>">
		  	<i class="reply icon"></i>
		</a>
	</span>
	<h1 class="ui teal dividing header">Modificar Evento</h1>
	<form action="<?php echo base_url()?>/admin/evento/modificarEvento/<?php echo $evento['idEvento'];?>/1" method="post" class="ui form" enctype="multipart/form-data" >
		<div class="ui two column grid margenmod">
			<div class="column">
				<div class="ui segment">
					<h5 class="ui teal centered header">Datos del Evento</h5>
					<div class="field">
						<label>Tipo de Evento</label>
						<select name="tipoEvento" id="tipoEvento">
							<?php foreach ($tiposEvento as $tipoEvento):
								$seleccionado=($tipoEvento['idTipoEvento']==$evento['idTipoEvento'])?"selected":"";
								echo "<option value=".$tipoEvento['idTipoEvento']." $seleccionado >".$tipoEvento['descripcionTipoEvento']."</option>";
							endforeach;	?>
						</select>
					</div>
					<div class="field">
						<label>Detalle de Evento</label>
						<input type="text" name="nombreEvento" id="nombreEvento" value="<?php echo $evento['tituloEvento']?>">
					</div>
					<div class="field">
						<label>Descripción de Evento</label>
		    		<textarea placeholder="Curso de redaccion periodistica" name="descripcionEvento" id="descripcionEvento"><?php echo $evento['descripcionEvento']; ?></textarea>
						<script>
							CKEDITOR.replace( 'descripcionEvento' );
						</script>
					</div>
					<div class="field" id="fechaInicio">
						<label>Fecha de Inicio</label>
		    			<input type="text" name="fechaInicioEvento" id="fechaInicioEvento" value="<?php echo $evento['fechaInicioEvento']?>">
					</div>
					<div class="field" id="fechaFin">
						<label>Fecha de Fin</label>
		    			<input type="text" name="fechaFinEvento" id="fechaFinEvento" value="<?php echo $evento['fechaFinEvento']?>">
					</div>
					<div class="field" id="certificado">
						<label>Certificado</label>
					</div>
					<?php
					$leyenda="Se entrega";
					if ($evento['plantillaCertificadoEvento']){
						$leyenda="Modificar";
					?>
						<?php if ($evento['plantillaCertificadoEvento']!=null):?>
						<a href="<?php echo base_url('application/cargasDocumentos/plantillasCertificado/'.$evento['plantillaCertificadoEvento'])?>" >
							<img class="ui image fluid bordered" id="imagenCertificado" src="<?php echo base_url('application/cargasDocumentos/plantillasCertificado/PDF/'.$evento['miniatura']);?>">
						</a>
						<?php endif;?>
					<br>
					<?php
					}
					?>
					<div class="field" id="cert">
						<div class="ui checkbox" id="certificadoEvento">
						  <input type="checkbox" name="certificadoEvento" id="certificadoCheckbox" onclick="requiereCertificadoMod(this)" >
						  <label><?php echo $leyenda?> certificado</label>
						</div>
						<br>
					</div>

					<div class="field" id="documentoCertificado" style="display: none;">
						<label>Escoja el Certificado</label>
						<input type="file" name="plantillaEvento" id="documento" accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
					</div>
					<div class="field" id="imagen">
						<label>Imagen</label>
						<img src="<?php echo base_url('application/cargasDocumentos/imagenes/'.$evento['imagenEvento']); ?>" class="ui small image">
						<input type="file" name="imagenEvento" id="imagenEvento" accept="image/*"/>
					</div>
					<br><div class="field" id="cupo">
						<label>Cupo (0 es Ilimitado)</label>
						<input type="number" placeholder="Nombre" name="cupoEvento" value="<?php echo $evento['cupoEvento']?>">
					</div>
				</div>
			</div>
			<div class="column">
				<div class="ui segment">
					<div class="field" id="tipoParticipante">
						<h5 class="ui teal centered header">Costos</h5>
					</div>
					<h5 class="ui dividing teal header">Tipo de Participante</h5>
					<?php
					foreach ($tiposParticipante as $tipoParticipante):?>
					<div class="field" id="tipoParticipante<?php echo $tipoParticipante['idTipoParticipante'];?>">
						  <label><?php echo $tipoParticipante['descripcionTipoParticipante'].' $'.$tipoParticipante['costoValorEvento']?></label>
					</div>
					<?php endforeach;?>
				</div>
				<br><div class="field" id="observaciones">
						<label>Observaciones:</label>
						<textarea name="observacionesModificacion" id="observacionesModificacion" rows="4" cols="70"><?php if($evento['observacionesModificacion']!=null) echo $evento['observacionesModificacion'];?></textarea>
					</div>
			</div>
		</div>
		<br>
		<div class="margenmod" style="text-align: center">
			<input type="submit" class="ui teal button" value="Guardar"/>&nbsp;&nbsp;
			<a href="<?php echo base_url('/admin/evento')?>" class="ui button">Cancelar</a>
		</div>
	</form>
</div>
