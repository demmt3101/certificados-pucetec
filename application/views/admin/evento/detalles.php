<br>
<script type="text/javascript" src="<?php echo base_url('application/assets/js/certificado.js') ?>"></script>
<div class="ui container segment">
	<a href="<?php echo base_url('/admin/evento/');?>">
	<span class="ui icon button" style="float:right">
  		<i class="reply icon"></i>
	</span>
	</a>
	<?php  if ($aModificar){?>
	<a href="<?php echo base_url('/admin/evento/modificarEvento/'.$evento['idEvento']);?>">
	<span class="ui green icon button" style="float:right">
  		<i class="edit icon"></i>
	</span>
	</a>
	<?php } ?>
	<h1 class="ui teal dividing header"><?php echo $evento['tituloEvento']?></h1>
	<div class="ui two column grid">
		<div class="column">
			<h3 class="ui teal header">Fechas</h3>
			<?php
				$fechaInicioEvento = new DateTime($evento['fechaInicioEvento']);
				$fechaInicioEvento = new Carbon\Carbon($fechaInicioEvento->format(DATE_ISO8601));
				$fechaFinEvento = new DateTime($evento['fechaFinEvento']);
				$fechaFinEvento = new Carbon\Carbon($fechaFinEvento->format(DATE_ISO8601));
			 ?>
			<p><strong>Fecha y Hora de Inicio: </strong><?php echo $fechaInicioEvento->formatLocalized('%A, %d de %B de %Y'); ?><br></p>
			<p><strong>Fecha y Hora de Finalización: </strong><?php echo $fechaFinEvento->formatLocalized('%A, %d de %B de %Y'); ?></p><br><br>
			<p><strong>Se entrega certificado: </strong><?php if ($evento['certificadoEvento']==0) echo 'No'; else echo 'Sí';?></p>
			<div>
				<strong>Descripción:</strong><br>
				<div><?php echo $evento['descripcionEvento'] ?></div>
			</div>
		</div>
		<div class="column">
			<h3 class="ui teal header">Tipos de participante</h3>
			<?php foreach ($costos as $costo):?>
				<p><strong><?php echo $costo['descripcionTipoParticipante'];?>: </strong>
					<?php if($costo['costoValorEvento']==0)
								echo "gratuito.";
							else
								echo '$ '.number_format($costo['costoValorEvento'],2).' USD.';
					?>
				</p>
			<?php endforeach;?>
		</div>
	</div>
</div>
<?php if ($participantes==null):?>
	<div class="ui container">
		<div class="ui red segment">
			<div class="ui centered header"><i class="ui frown icon"></i>No existen inscritos en este curso</div>
		</div>
	</div>
	<?php else:?>
	<div class="ui container segment">
		<h3 class="ui dividing header">Participantes</h3>

		<div class="ui teal inverted segment">
			<p>Participantes Matriculados: <strong><?php echo $numeroParticipantes?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Participantes Confirmados: <strong><?php echo $participantesConfirmados?></strong></p>
		</div>

		<?php if ($this->session->userdata('idTipoEmpleado')!=4):?>
		<div class="ui center aligned teal segment form">
			<div class="inline field">
			 <label>Solapín</label>
			 <input onchange="registarAsistencia(this.value,'<?php echo $this->session->userdata('idEmpleado') ?>');this.value='';this.focus()" type="text" placeholder="Pasar el lector en el solapín" >
			<?php if ($this->evento_model->esGratuito($idEvento)):?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('/admin/evento/aprobarParticipantesEvento/'.$idEvento)?>"><i class="big teal users icon"></i></a>
			<?php endif;?>
			</div>
		</div>

		<?php endif;?>
		<table id="participantes" class="ui selectable definition table">
			<thead>
				  <tr>
				  	<th></th>
				    <th>Apellidos</th>
				    <th>Nombres</th>
				    <th>Email</th>
				    <th>Institución</th>
				    <th>Teléfono</th>
				    <th>Acciones</th>
				  </tr>
			</thead>
			<tbody>
			  	<?php
				  $i=0;
				  foreach ($participantes as $participante):?>
				  <tr <?php if($participante['estadoParticipanteEvento']!=1) echo 'class="negative"'; else echo 'class="positive"';?>>
				  	  <td><?php echo ++$i;?></td>
				      <td id="apellidos<?php echo $participante['idParticipanteEvento'];?>"><?php echo $participante['apellidosParticipante']?></td>
				      <td id="nombres<?php echo $participante['idParticipanteEvento'];?>"><?php echo $participante['nombresParticipante']?></td>
				      <td id="email<?php echo $participante['idParticipanteEvento'];?>"><?php echo $participante['correoElectronicoParticipante']?></td>
				      <td id="institucion<?php echo $participante['idParticipanteEvento'];?>"><?php echo $participante['institucionParticipante']?></td>
				      <td id="telefono<?php echo $participante['idParticipanteEvento'];?>"><?php echo $participante['telefonoParticipante']?></td>

				      <td>
				      	<a title="Detalles de participante" href="<?php echo base_url('/admin/participante/detalles/'.$participante['idParticipante']).'/'.$evento['idEvento'];?>"><i class="big teal user icon"></i></a>
				     	<a title="Imprimir solapín" href="#" onclick="desplegarSolapin(<?php echo $participante['idParticipanteEvento']?>)"><i class="big teal tag icon"></i></a>
				     	<a title="Registrar Asistencia" href="#participantes" onclick="desplegarConfirmacionAsistencia(<?php echo $participante['idParticipanteEvento'] ?>, <?php echo $this->session->userdata('idEmpleado') ?>)"><i class="big teal check circle icon"></i></a>
				     	<?php if($participante['confirmadoPorParticipanteEvento']!=0 && $participante['estadoParticipanteEvento']==1 && $this->session->userdata('idTipoEmpleado')!=4) :?>
				     	<?php if ($evento['certificadoEvento']!=0):?>
				     	<a title="Entregar certificado" href="#" onclick="mostrarCertificado(<?php echo $participante['idParticipanteEvento'] ?>)"><i class="big teal student icon"></i></a>
				     	<?php endif;?>
				     	<?php endif;?>
				     	<a title="Historial" href="<?php echo base_url('/admin/asistencia/historial/'.$participante['idParticipante'].'/'.$evento['idEvento'].'/'.$participante['idParticipanteEvento'])?>">
				     		<i class="big teal history icon"></i>
				     	</a>
				      </td>
				  </tr>
				  <?php endforeach;?>
			</tbody>
		</table>
	</div>
	<?php endif;?>

	<div id="mensaje" class="ui yellow inverted center aligned segment" style="position: fixed; top: 45%; width: 100%; display: none; z-index: 10 !important;">Registo de asistencia Exitoso</div>
	<div id="mensajeError" class="ui red inverted center aligned segment" style="position: fixed; top: 45%; width: 100%; display: none; z-index: 10 !important;">ERROR: Este usuario no pertenece a este evento o no está confirmado</div>
<script>

$(document).ready(function(){
    $("#participantes").DataTable( {
    	"columnDefs": [
    	               { "orderable": false, "targets": 0 }
    	],
    	"columns": [
        	null,
    	    null,
    	    null,
    	    null,
    	    { "width": "10%" },
    	    null,
    	    null,
    	  ],
    	scrollY:        "60vh",
    	deferRender:    true,
        scrollCollapse: true,
        deferRender:    false,
        scroller:       true,
    	language: {
    		"sProcessing":     "Procesando...",
    	    "sLengthMenu":     "Mostrar _MENU_ registros",
    	    "sZeroRecords":    "No se encontraron resultados",
    	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    	    "sInfoPostFix":    "",
    	    "sSearch":         "<i class=\"search icon\"></i>",
    	    "sUrl":            "",
    	    "sInfoThousands":  ",",
    	    "sLoadingRecords": "Cargando...",
    	    "oPaginate": {
    	        "sFirst":    "Primero",
    	        "sLast":     "Último",
    	        "sNext":     "Siguiente",
    	        "sPrevious": "Anterior"
    	    }
        }
    });
  });

function desplegarConfirmacionAsistencia(idParticipanteEvento, idEmpleado){
	$('.ui.basic.modal').remove();
	$('#contenedor').append(''+
		'<div class="ui basic modal">'+
			'<i class="close icon"></i>'+
			'<div class="header">Registrar asistencia</div>'+
			'<div class="image content">'+
			    '<div class="image">'+
			      '<i class="check icon"></i>'+
			    '</div>'+
			    '<div id="descripcionAsistencia" class="description">'+
			    	'<p>Está seguro que desea registrar la asistencia para: <strong>' +$('#nombres'+idParticipanteEvento).text() +' '+ $('#apellidos'+idParticipanteEvento).text() +'</strong></p>'+
			    '</div>'+
			'</div>'+
			'<div class="actions">'+
			    '<div class="two fluid ui inverted buttons">'+
			      '<div class="ui red basic inverted button" onclick="$(\'.ui.basic.modal\').modal(\'hide\')">'+
			        '<i class="remove icon"></i>'+
			        'No'+
			      '</div>'+
			      '<div class="ui green basic inverted button" onclick="registarAsistencia('+idParticipanteEvento + ','+ idEmpleado +')">'+
			        '<i class="checkmark icon"></i>'+
			        'Sí'+
			      '</div>'+
			    '</div>'+
			  '</div>'+
		'</div>');
	$('.ui.basic.modal')
	  .modal('show')
	;
}

function registarAsistencia(idParticipanteEvento, idEmpleado){
	$.ajax({
		type:"POST",
		dataType:"html",
		url: '<?php echo base_url('/admin/asistencia/registrarAsistencia/'.$evento['idEvento'])?>',
		data:"datos="+idParticipanteEvento+"<asistencia>"+idEmpleado,
		success:function(msg){
			if(msg==""||msg=="false"){
				$('#descripcionAsistencia').append('<div style="font-color:red z-index:10">Error</div>')
				$('.ui.basic.modal').transition('shake');
				$('#mensajeError').transition('fade','1000ms').delay(9000).transition('fade', '1000ms');
			}else{
				$('#mensaje').transition('fade','1000ms').delay(9000).transition('fade', '1000ms');
				$('.ui.basic.modal').modal('hide');
			}
		}
	});
}


</script>
