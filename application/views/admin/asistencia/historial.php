<div class="ui container segment">
	<a href="<?php echo base_url('/admin/evento/detalles/'.$evento['idEvento']);?>">
	<span class="ui icon button iconmargen">
  		<i class="reply icon"></i>
	</span>
	</a>
	<h1 class="ui teal dividing header"><?php echo $evento['tituloEvento']?></h1>
	<div class="ui two column grid">
		<div class="column">
			<h3 class="ui teal header">Fechas</h3>
			<p><strong>Fecha y Hora de Inicio de Evento: </strong><?php echo $evento['fechaInicioEvento']?><br></p>
			<p><strong>Fecha y Hora de Finalización de Evento: </strong><?php echo $evento['fechaFinEvento']?></p><br><br>
			<p><strong>Requiere certificado: </strong><?php if ($evento['certificadoEvento']==0) echo 'No'; else echo 'Sí';?></p>
		</div>
		<div class="column">
			&nbsp;
		</div>
	</div>
	<?php if ($historial==null):?>
	<p>Este participante NO registra ninguna asistencia a esta evento</p>
	<?php else:?>
	<h3 class="ui teal dividing header">Ingresos registrados para <?php echo $participante['nombresParticipante'].' '.$participante['apellidosParticipante']?></h3>
		<table id="participantes" class="ui selectable definition table">
			<thead>
				  <tr>
				  	<th></th>
				    <th>Fecha de ingreso</th>
				    <th>Registrado por</th>
				  </tr>
			</thead>
			<tbody>
			  	<?php
				  $i=0;
				  foreach ($historial as $asistencia):?>
				  <tr>
						<td><?php echo ++$i;?></td>
						<td><?php echo $asistencia['fechaAsistencia']?></td>
						<td><?php echo $asistencia['nombresEmpleado'].' '.$asistencia['apellidosEmpleado']?>
				  </tr>
				  <?php endforeach;?>
			</tbody>
		</table>
	<?php endif;?>
</div>
