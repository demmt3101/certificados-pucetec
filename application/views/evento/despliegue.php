<?php
/**
 * muestra evento con Login para participante
 */
?>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="icon" href="<?php echo base_url("application/assets/img/favicon.ico.png")?>" type="image/png">
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/jquery-2.1.3.min.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/semantic.css') ?>">
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<script src="<?php echo base_url('application/assets/semantic-ui/semantic.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('application/assets/js/functions.js') ?>"></script>
		<link rel="stylesheet/less" type="text/css" href="<?php echo base_url('application/assets/semantic-ui/src/definitions/collections/menu.less') ?>">
		<link rel="stylesheet" href="<?php echo base_url('application/assets/custom.css') ?>" type="text/css">
		<title>Sistema de Inscripción</title>
	</head>
	<body>
		<div class="ui container">
			<br>
			<h2 class="ui teal center aligned dividing header content">Evento: <?php echo $evento['tituloEvento']?></h2>
			<div class="ui stackable two column grid">
				<div class="column">
					<div class="ui teal segment">
						<div class="ui card" style="width:100%">
						  <div class="image">
						    <img src="<?php echo base_url('application/cargasDocumentos/imagenes/'.$evento['imagenEvento'])?>">
						  </div>
						  <div class="content">
								<?php
								 	$fechaInicioEvento = new DateTime($evento['fechaInicioEvento']);
									$fechaInicioEvento = new Carbon\Carbon($fechaInicioEvento->format(DATE_ISO8601));
		 						 	$fechaFinEvento = new DateTime($evento['fechaFinEvento']);
		 							$fechaFinEvento = new Carbon\Carbon($fechaFinEvento->format(DATE_ISO8601));
		 						 ?>
						    <div class="meta">
						      <span class="date">
										Desde: <?php echo $fechaInicioEvento->formatLocalized('%A, %d de %B de %Y'); ?>
										hasta: <?php echo $fechaFinEvento->formatLocalized('%A, %d de %B de %Y'); ?>
									</span>
						    </div>
						    <div class="description">
									<?php echo $evento['descripcionEvento']; ?>
								</div>
						  </div>
							<?php if ($numeroParticipantes): ?>
							  <div class="extra content">
							    <a>
							      <i class="user icon"></i>
							      <?php echo $numeroParticipantes ?> Participantes
							    </a>
							  </div>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<?php if ($this->session->userdata('nombresParticipante') == null): ?>
					<div class="center aligned column">
					<h3 class="ui teal center aligned dividing header content"> Inicio de sesión </h3>
						<form action="<?php echo base_url('evento/'.$evento['idEvento'])?>" method="post" class="ui large form">
							<div class="ui stacked segment">
								<div class="field">
									<div class="ui left icon input">
										<i class="user icon"></i>
										<input type="text" name="email" placeholder="Usuario">
									</div>
								</div>
								<div class="field">
									<div class="ui left icon input">
										<i class="lock icon"></i>
										<input type="password" name="contrasena" placeholder="Contraseña">
									</div>
								</div>
								<input type="hidden" name="idEvento" id="idEvento" value="<?php echo $evento['idEvento']?>">
								<input type="submit" value="Ingresar" class="ui primary button">
							</div>
						</form>
						<?php if (isset($error)): ?>
							<div class="ui error message"><?php echo $error;?></div>
						<?php endif; ?>
						<div>
		  						<a class="ui teal basic button" href="<?php echo base_url('/participante/primerRegistro/'.$evento['idEvento']);?>">Registrarse</a>
						</div>
						<div><br />
							<a href="<?php echo base_url('/participante/recuperarContrasena')?>" class="ui teal sub header">Recuperar Contraseña</a>
						</div>
					</div>
				<?php else: ?>
					<div class="column">
						<div class="ui yellow segment">
							<p>
								<strong><?php echo $this->session->userdata('nombresParticipante'); ?></strong>,<br>
								Estás a punto de inscibirte al evento: <strong><?php echo $evento['tituloEvento']?></strong>.
							</p>
							<p>Para confirmar esta acción, por favor haz clic en el botón a continuación</p>

							<div style="text-align:center">
								<i class="huge yellow hand point down icon"></i><br><br>
								<form action="<?php echo base_url('evento/inscribir')?>" method="post">
									<input type="hidden" name="idEvento" id="idEvento" value="<?php echo $evento['idEvento']?>">
									<input type="submit" value="Inscribir" class="ui primary button">
								</form>
							</div>
						</div>
					</div>
				<?php endif;  ?>
			</div>
		</div>
	</body>
</html>
